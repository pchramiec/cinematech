﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.Windows;

namespace Cinematech
{

    class Contact
    {
        MainPage mpg;
        public Contact(MainPage mpg)
        {

            this.mpg = mpg;
        }

        public void wyslij()
        {
            try
            {
                if ((mpg.textBox_Contact_Email.Text.Length != 0) && (mpg.textBox_Contact_Name.Text.Length != 0) && (mpg.textBox_Contact_Content.Text.Length != 0) && (mpg.textBox_Contact_Email_reply.Text.Length != 0))
                {
                    mpg.button_Contact_Send.IsEnabled = false;
                    MailMessage wiadomosc = new MailMessage();
                    wiadomosc.From = new MailAddress("cinematech@onet.pl");
                    wiadomosc.Subject = mpg.textBox_Contact_Name.Text + " - " + mpg.textBox_Contact_Email.Text;
                    wiadomosc.Body = mpg.textBox_Contact_Content.Text + "\n\nAdres zwrotny: " + mpg.textBox_Contact_Email_reply.Text;
                    wiadomosc.To.Add("cinematech@onet.pl");
                    var eMailValidator = new System.Net.Mail.MailAddress(mpg.textBox_Contact_Email_reply.Text);

                    SmtpClient client = new SmtpClient();
                    client.Credentials = new NetworkCredential("cinematech@onet.pl", "Cinematech2017");
                    client.Host = "smtp.poczta.onet.pl";
                    client.Port = 587;
                    client.EnableSsl = false;
                    client.Send(wiadomosc);
                    MessageBox.Show("Wysłano wiadomość", "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);
                    mpg.textBox_Contact_Email_reply.Clear();
                    mpg.textBox_Contact_Name.Clear();
                    mpg.textBox_Contact_Email.Clear();
                    mpg.textBox_Contact_Content.Clear();
                }
                else MessageBox.Show("Wypełnij wszystkie pola!", "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Wystąpił niespodziewany błąd! \r \n" + ex.Message.ToString(), "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                mpg.button_Contact_Send.IsEnabled = true;
            }

        }
    }
}
