﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Data;



namespace Cinematech
{
    class Function
    {
        MainPage mainpg;
        public Function(MainPage mainpg)
        {
            this.mainpg = mainpg;
        }
        public string DataFormat()
        {
            string data;
            DateTime Time_C = DateTime.Now;
            data = Time_C.ToString("dd/MM/yyyy HH:mm:ss");
            return data;
        }
    }
}
