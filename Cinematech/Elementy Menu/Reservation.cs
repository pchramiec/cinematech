﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.Data;
using System.Windows;
using System.Windows.Media;

namespace Cinematech
{
    class Reservation
    {
        MainPage mpg;
        public Reservation(MainPage mpg)
        {
            this.mpg = mpg;
        }

        public static string RandomString(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        private DateTime GetDate(DateTime d)
        {
            return new DateTime(d.Year, d.Month, d.Day);
        }
        public void MakeReservation(OleDbConnection myConnect)
        {
            try
            {
                //Wysyłanie danych z rezerwacji do bazy danych
                if ((mpg.textBox_Reservation_FirstName.Text.Length) == 0 || (mpg.textBox_Reservation_Email.Text.Length == 0) || (mpg.comboBox_Reservation_Movie.SelectedIndex == -1))
                {
                    MessageBox.Show("Wypełnij wszystkie pola!", "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    var eMailValidator = new System.Net.Mail.MailAddress(mpg.textBox_Reservation_Email.Text);
                    string text = RandomString(8);
                    MessageBox.Show("Twój kod rezerwacji to: " + text, "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);
                    myConnect.Open();
                    OleDbCommand cmd = new OleDbCommand("INSERT INTO REZERWACJA_MIEJSC ([Imie],[Email],[Film],[Godzina],[Sala],[Cena],[Tydzien],[Miejsce],[Dzien],[Kod],[Data_Rez]) VALUES (@nam,@sur,@mov,@hou,@room,@prize,@dater,@place,@dayr, @kod,@date_r)", myConnect);
                    cmd.Parameters.AddWithValue("@nam", mpg.textBox_Reservation_FirstName.Text);
                    cmd.Parameters.AddWithValue("@sur", mpg.textBox_Reservation_Email.Text);
                    cmd.Parameters.AddWithValue("@mov", mpg.comboBox_Reservation_Movie.Text);
                    cmd.Parameters.AddWithValue("@hou", mpg.comboBox_Reservation_Time.Text);
                    cmd.Parameters.AddWithValue("@room", mpg.comboBox_Reservation_Room.Text);
                    cmd.Parameters.AddWithValue("@prize", mpg.comboBox_Reservation_Prize.Text);
                    cmd.Parameters.AddWithValue("@dater", mpg.comboBox_Reservation_Date.Text);
                    cmd.Parameters.AddWithValue("@place", mpg.comboBox_Reservation_Seat.Text);
                    cmd.Parameters.AddWithValue("@dayr", mpg.comboBox_Day_Of_Week.Text);
                    cmd.Parameters.AddWithValue("@kod", text.ToString());
                    cmd.Parameters.AddWithValue("@date_r",GetDate(DateTime.Now));
                    cmd.ExecuteNonQuery();
                    myConnect.Close();
                    myConnect.Open();
                    OleDbCommand cmd2 = new OleDbCommand("INSERT INTO DANE ([E-mail]) VALUES (@email)", myConnect);
                    cmd2.Parameters.AddWithValue("@email", mpg.textBox_Reservation_Email.Text);
                    cmd2.ExecuteNonQuery();
                    myConnect.Close();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Wystąpił niespodziewany błąd! \r \n" + ex.Message.ToString(), "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        public void InitCombo(OleDbConnection myConnect)
        {
            //Dodanie do Combobox godzin seanów
            try
            {
                //Dodanie do Combobox sal
                if (mpg.comboBox_Reservation_Room.Items.IsEmpty)
                {
                    mpg.comboBox_Reservation_Room.Items.Clear();
                    mpg.comboBox_Reservation_Room.Items.Add("Mała");
                    mpg.comboBox_Reservation_Room.Items.Add("Średnia");
                    mpg.comboBox_Reservation_Room.Items.Add("Duża");
                    mpg.comboBox_Reservation_Room.SelectedIndex = 0;
                }

                if (mpg.comboBox_Reservation_Room.SelectedIndex == 0)
                {
                    //Dodanie do Combobox miejsc z małej sali
                    mpg.comboBox_Reservation_Seat.Items.Clear();
                    mpg.comboBox_Reservation_Seat.Items.Add("A2");
                    mpg.comboBox_Reservation_Seat.Items.Add("A3");
                    mpg.comboBox_Reservation_Seat.Items.Add("A4");
                    mpg.comboBox_Reservation_Seat.Items.Add("A5");
                    mpg.comboBox_Reservation_Seat.Items.Add("A6");
                    mpg.comboBox_Reservation_Seat.Items.Add("A7");
                    mpg.comboBox_Reservation_Seat.Items.Add("A8");
                    mpg.comboBox_Reservation_Seat.Items.Add("B1");
                    mpg.comboBox_Reservation_Seat.Items.Add("B2");
                    mpg.comboBox_Reservation_Seat.Items.Add("B3");
                    mpg.comboBox_Reservation_Seat.Items.Add("B4");
                    mpg.comboBox_Reservation_Seat.Items.Add("B5");
                    mpg.comboBox_Reservation_Seat.Items.Add("C1");
                    mpg.comboBox_Reservation_Seat.Items.Add("C2");
                    mpg.comboBox_Reservation_Seat.Items.Add("C3");
                    mpg.comboBox_Reservation_Seat.Items.Add("C4");
                    mpg.comboBox_Reservation_Seat.Items.Add("C5");
                    mpg.comboBox_Reservation_Seat.Items.Add("D1");
                    mpg.comboBox_Reservation_Seat.Items.Add("D2");
                    mpg.comboBox_Reservation_Seat.Items.Add("D3");
                    mpg.comboBox_Reservation_Seat.SelectedIndex = 0;
                }
                if (mpg.comboBox_Reservation_Room.SelectedIndex == 1)
                {
                    //Dodanie do Combobox miejsc z dużej sali
                    mpg.comboBox_Reservation_Seat.Items.Clear();

                    mpg.comboBox_Reservation_Seat.Items.Add("A1");
                    mpg.comboBox_Reservation_Seat.Items.Add("A2");
                    mpg.comboBox_Reservation_Seat.Items.Add("A3");
                    mpg.comboBox_Reservation_Seat.Items.Add("A4");
                    mpg.comboBox_Reservation_Seat.Items.Add("A5");
                    mpg.comboBox_Reservation_Seat.Items.Add("A6");
                    mpg.comboBox_Reservation_Seat.Items.Add("A7");
                    mpg.comboBox_Reservation_Seat.Items.Add("A8");
                    mpg.comboBox_Reservation_Seat.Items.Add("B1");
                    mpg.comboBox_Reservation_Seat.Items.Add("B2");
                    mpg.comboBox_Reservation_Seat.Items.Add("B3");
                    mpg.comboBox_Reservation_Seat.Items.Add("B4");
                    mpg.comboBox_Reservation_Seat.Items.Add("B5");
                    mpg.comboBox_Reservation_Seat.Items.Add("B6");
                    mpg.comboBox_Reservation_Seat.Items.Add("B7");
                    mpg.comboBox_Reservation_Seat.Items.Add("C1");
                    mpg.comboBox_Reservation_Seat.Items.Add("C2");
                    mpg.comboBox_Reservation_Seat.Items.Add("C3");
                    mpg.comboBox_Reservation_Seat.Items.Add("C4");
                    mpg.comboBox_Reservation_Seat.Items.Add("C5");
                    mpg.comboBox_Reservation_Seat.Items.Add("C6");
                    mpg.comboBox_Reservation_Seat.Items.Add("C7");
                    mpg.comboBox_Reservation_Seat.Items.Add("C8");
                    mpg.comboBox_Reservation_Seat.Items.Add("D1");
                    mpg.comboBox_Reservation_Seat.Items.Add("D2");
                    mpg.comboBox_Reservation_Seat.Items.Add("D3");
                    mpg.comboBox_Reservation_Seat.Items.Add("D4");
                    mpg.comboBox_Reservation_Seat.Items.Add("D5");
                    mpg.comboBox_Reservation_Seat.Items.Add("D6");
                    mpg.comboBox_Reservation_Seat.Items.Add("D7");
                    mpg.comboBox_Reservation_Seat.Items.Add("D8");
                    mpg.comboBox_Reservation_Seat.SelectedIndex = 0;
                }
                if (mpg.comboBox_Reservation_Room.SelectedIndex == 2)
                {
                    //Dodanie do Combobox miejsc z dużej sali
                    mpg.comboBox_Reservation_Seat.Items.Clear();
                    mpg.comboBox_Reservation_Seat.Items.Add("A1");
                    mpg.comboBox_Reservation_Seat.Items.Add("A2");
                    mpg.comboBox_Reservation_Seat.Items.Add("A3");
                    mpg.comboBox_Reservation_Seat.Items.Add("A4");
                    mpg.comboBox_Reservation_Seat.Items.Add("A5");
                    mpg.comboBox_Reservation_Seat.Items.Add("A6");
                    mpg.comboBox_Reservation_Seat.Items.Add("A7");
                    mpg.comboBox_Reservation_Seat.Items.Add("A8");
                    mpg.comboBox_Reservation_Seat.Items.Add("A9");
                    mpg.comboBox_Reservation_Seat.Items.Add("B1");
                    mpg.comboBox_Reservation_Seat.Items.Add("B2");
                    mpg.comboBox_Reservation_Seat.Items.Add("B3");
                    mpg.comboBox_Reservation_Seat.Items.Add("B4");
                    mpg.comboBox_Reservation_Seat.Items.Add("B5");
                    mpg.comboBox_Reservation_Seat.Items.Add("B6");
                    mpg.comboBox_Reservation_Seat.Items.Add("B7");
                    mpg.comboBox_Reservation_Seat.Items.Add("B8");
                    mpg.comboBox_Reservation_Seat.Items.Add("C1");
                    mpg.comboBox_Reservation_Seat.Items.Add("C2");
                    mpg.comboBox_Reservation_Seat.Items.Add("C3");
                    mpg.comboBox_Reservation_Seat.Items.Add("C4");
                    mpg.comboBox_Reservation_Seat.Items.Add("C5");
                    mpg.comboBox_Reservation_Seat.Items.Add("C6");
                    mpg.comboBox_Reservation_Seat.Items.Add("C7");
                    mpg.comboBox_Reservation_Seat.Items.Add("D1");
                    mpg.comboBox_Reservation_Seat.Items.Add("D2");
                    mpg.comboBox_Reservation_Seat.Items.Add("D3");
                    mpg.comboBox_Reservation_Seat.Items.Add("D4");
                    mpg.comboBox_Reservation_Seat.Items.Add("D5");
                    mpg.comboBox_Reservation_Seat.Items.Add("D6");
                    mpg.comboBox_Reservation_Seat.Items.Add("E1");
                    mpg.comboBox_Reservation_Seat.Items.Add("E2");
                    mpg.comboBox_Reservation_Seat.Items.Add("E3");
                    mpg.comboBox_Reservation_Seat.Items.Add("E4");
                    mpg.comboBox_Reservation_Seat.Items.Add("E5");
                    mpg.comboBox_Reservation_Seat.Items.Add("F1");
                    mpg.comboBox_Reservation_Seat.Items.Add("F2");
                    mpg.comboBox_Reservation_Seat.Items.Add("F3");
                    mpg.comboBox_Reservation_Seat.Items.Add("F4");
                    mpg.comboBox_Reservation_Seat.Items.Add("G1");
                    mpg.comboBox_Reservation_Seat.Items.Add("G2");
                    mpg.comboBox_Reservation_Seat.Items.Add("G3");
                    mpg.comboBox_Reservation_Seat.Items.Add("G4");
                    mpg.comboBox_Reservation_Seat.Items.Add("G5");
                    mpg.comboBox_Reservation_Seat.SelectedIndex = 0;
                }
                //Ustawienie cen w Combobox
                if (mpg.comboBox_Reservation_Prize.Items.IsEmpty)
                {
                    mpg.comboBox_Reservation_Prize.Items.Clear();
                    mpg.comboBox_Reservation_Prize.Items.Add("14.00 zł");
                    mpg.comboBox_Reservation_Prize.Items.Add("18.00 zł");
                    mpg.comboBox_Reservation_Prize.SelectedIndex = 1;

                }
               
                if (mpg.comboBox_Reservation_Date.Items.IsEmpty)
                {
                    mpg.comboBox_Reservation_Date.Items.Clear();
                    mpg.comboBox_Reservation_Date.Items.Add("Bieżący");
                    mpg.comboBox_Reservation_Date.Items.Add("Następny");
                    mpg.comboBox_Reservation_Date.SelectedIndex = 0;
                }

                DateTime today_temp = new DateTime();
                today_temp = DateTime.Now;
                int val = Convert.ToInt32(today_temp.DayOfWeek);
                if (mpg.comboBox_Day_Of_Week.Items.IsEmpty)
                {
                    if (mpg.comboBox_Reservation_Date.SelectedIndex == 0)
                    {
                        mpg.comboBox_Day_Of_Week.Items.Clear();
                        switch (val)
                        {
                            case 1:
                                {
                                    mpg.comboBox_Day_Of_Week.Items.Add("Poniedziałek");
                                    goto case 2;
                                }
                            case 2:
                                {
                                    mpg.comboBox_Day_Of_Week.Items.Add("Wtorek");
                                    goto case 3;
                                }
                            case 3:
                                {
                                    mpg.comboBox_Day_Of_Week.Items.Add("Środa");
                                    goto case 4;
                                }
                            case 4:
                                {
                                    mpg.comboBox_Day_Of_Week.Items.Add("Czwartek");
                                    goto case 5;
                                }
                            case 5:
                                {
                                    mpg.comboBox_Day_Of_Week.Items.Add("Piątek");
                                    goto case 6;
                                }
                            case 6:
                                {
                                    mpg.comboBox_Day_Of_Week.Items.Add("Sobota");
                                    goto case 0;
                                }
                            case 0:
                                {
                                    mpg.comboBox_Day_Of_Week.Items.Add("Niedziela");
                                }
                                break;
                            default:
                                break;
                        }

                    }
                    if (mpg.comboBox_Reservation_Date.SelectedIndex == 1)
                    {
                        mpg.comboBox_Day_Of_Week.Items.Clear();
                        mpg.comboBox_Day_Of_Week.Items.Add("Poniedziałek");
                        mpg.comboBox_Day_Of_Week.Items.Add("Wtorek");
                        mpg.comboBox_Day_Of_Week.Items.Add("Środa");
                        mpg.comboBox_Day_Of_Week.Items.Add("Czwartek");
                        mpg.comboBox_Day_Of_Week.Items.Add("Piątek");
                        mpg.comboBox_Day_Of_Week.Items.Add("Sobota");
                        mpg.comboBox_Day_Of_Week.Items.Add("Niedziela");
                    }
                    mpg.comboBox_Day_Of_Week.SelectedIndex = 0;
                }

                if (mpg.comboBox_Reservation_Date.SelectedIndex == 0 && mpg.comboBox_Day_Of_Week.Text.Equals("Poniedziałek"))
                {
                    mpg.comboBox_Reservation_Movie.Items.Clear();
                    foreach (DataRowView row in mpg.Timetable_Mon.Items)
                    {
                        mpg.comboBox_Reservation_Movie.Items.Add(row.Row[0].ToString());
                    }
                }
                if (mpg.comboBox_Reservation_Date.SelectedIndex == 0 && mpg.comboBox_Day_Of_Week.Text.Equals("Wtorek"))
                {
                    mpg.comboBox_Reservation_Movie.Items.Clear();
                    foreach (DataRowView row in mpg.Timetable_Tue.Items)
                    {
                        mpg.comboBox_Reservation_Movie.Items.Add(row.Row[0].ToString());
                    }
                }
                if (mpg.comboBox_Reservation_Date.SelectedIndex == 0 && mpg.comboBox_Day_Of_Week.Text.Equals("Środa"))
                {
                    mpg.comboBox_Reservation_Movie.Items.Clear();
                    foreach (DataRowView row in mpg.Timetable_Wed.Items)
                    {
                        mpg.comboBox_Reservation_Movie.Items.Add(row.Row[0].ToString());
                    }
                }
                if (mpg.comboBox_Reservation_Date.SelectedIndex == 0 && mpg.comboBox_Day_Of_Week.Text.Equals("Czwartek"))
                {
                    mpg.comboBox_Reservation_Movie.Items.Clear();
                    foreach (DataRowView row in mpg.Timetable_Thu.Items)
                    {
                        mpg.comboBox_Reservation_Movie.Items.Add(row.Row[0].ToString());
                    }
                }
                if (mpg.comboBox_Reservation_Date.SelectedIndex == 0 && mpg.comboBox_Day_Of_Week.Text.Equals("Piątek"))
                {
                    mpg.comboBox_Reservation_Movie.Items.Clear();
                    foreach (DataRowView row in mpg.Timetable_Fri.Items)
                    {
                        mpg.comboBox_Reservation_Movie.Items.Add(row.Row[0].ToString());
                    }
                }
                if (mpg.comboBox_Reservation_Date.SelectedIndex == 0 && mpg.comboBox_Day_Of_Week.Text.Equals("Sobota"))
                {
                    mpg.comboBox_Reservation_Movie.Items.Clear();
                    foreach (DataRowView row in mpg.Timetable_Sat.Items)
                    {
                        mpg.comboBox_Reservation_Movie.Items.Add(row.Row[0].ToString());
                    }
                }
                if (mpg.comboBox_Reservation_Date.SelectedIndex == 0 && mpg.comboBox_Day_Of_Week.Text.Equals("Niedziela"))
                {
                    mpg.comboBox_Reservation_Movie.Items.Clear();
                    foreach (DataRowView row in mpg.Timetable_Sun.Items)
                    {
                        mpg.comboBox_Reservation_Movie.Items.Add(row.Row[0].ToString());
                    }
                }
                if (mpg.comboBox_Reservation_Date.SelectedIndex == 1 && mpg.comboBox_Day_Of_Week.SelectedIndex == 0)
                {
                    mpg.comboBox_Reservation_Movie.Items.Clear();
                    foreach (DataRowView row in mpg.Timetable_Mon2.Items)
                    {
                        mpg.comboBox_Reservation_Movie.Items.Add(row.Row[0].ToString());
                    }
                }
                if (mpg.comboBox_Reservation_Date.SelectedIndex == 1 && mpg.comboBox_Day_Of_Week.SelectedIndex == 1)
                {
                    mpg.comboBox_Reservation_Movie.Items.Clear();
                    foreach (DataRowView row in mpg.Timetable_Tue2.Items)
                    {
                        mpg.comboBox_Reservation_Movie.Items.Add(row.Row[0].ToString());
                    }
                }
                if (mpg.comboBox_Reservation_Date.SelectedIndex == 1 && mpg.comboBox_Day_Of_Week.SelectedIndex == 2)
                {
                    mpg.comboBox_Reservation_Movie.Items.Clear();
                    foreach (DataRowView row in mpg.Timetable_Wed2.Items)
                    {
                        mpg.comboBox_Reservation_Movie.Items.Add(row.Row[0].ToString());
                    }
                }
                if (mpg.comboBox_Reservation_Date.SelectedIndex == 1 && mpg.comboBox_Day_Of_Week.SelectedIndex == 3)
                {
                    mpg.comboBox_Reservation_Movie.Items.Clear();
                    foreach (DataRowView row in mpg.Timetable_Thu2.Items)
                    {
                        mpg.comboBox_Reservation_Movie.Items.Add(row.Row[0].ToString());
                    }
                }
                if (mpg.comboBox_Reservation_Date.SelectedIndex == 1 && mpg.comboBox_Day_Of_Week.SelectedIndex == 4)
                {
                    mpg.comboBox_Reservation_Movie.Items.Clear();
                    foreach (DataRowView row in mpg.Timetable_Fri2.Items)
                    {
                        mpg.comboBox_Reservation_Movie.Items.Add(row.Row[0].ToString());
                    }
                }
                if (mpg.comboBox_Reservation_Date.SelectedIndex == 1 && mpg.comboBox_Day_Of_Week.SelectedIndex == 5)
                {
                    mpg.comboBox_Reservation_Movie.Items.Clear();
                    foreach (DataRowView row in mpg.Timetable_Sat2.Items)
                    {
                        mpg.comboBox_Reservation_Movie.Items.Add(row.Row[0].ToString());
                    }
                }
                if (mpg.comboBox_Reservation_Date.SelectedIndex == 1 && mpg.comboBox_Day_Of_Week.SelectedIndex == 6)
                {
                    mpg.comboBox_Reservation_Movie.Items.Clear();
                    foreach (DataRowView row in mpg.Timetable_Sun2.Items)
                    {
                        mpg.comboBox_Reservation_Movie.Items.Add(row.Row[0].ToString());
                    }
                }
            }

            catch (Exception ex)
            {
            }
        }
        public void Resrv_Hour()
        {
            try
            {
                mpg.comboBox_Reservation_Time.Items.Clear();
                if (mpg.comboBox_Reservation_Movie.SelectedItem.ToString().Equals(mpg.comboBox_Reservation_Movie.Items[0].ToString()))
                {
                    mpg.comboBox_Reservation_Time.Items.Add("8:45");
                }
                if (mpg.comboBox_Reservation_Movie.SelectedItem.ToString().Equals(mpg.comboBox_Reservation_Movie.Items[1].ToString()))
                {

                    mpg.comboBox_Reservation_Time.Items.Add("12:00");
                }
                if (mpg.comboBox_Reservation_Movie.SelectedItem.ToString().Equals(mpg.comboBox_Reservation_Movie.Items[2].ToString()))
                {

                    mpg.comboBox_Reservation_Time.Items.Add("15:30");
                }
                if (mpg.comboBox_Reservation_Movie.SelectedItem.ToString().Equals(mpg.comboBox_Reservation_Movie.Items[3].ToString()))
                {

                    mpg.comboBox_Reservation_Time.Items.Add("17:30");
                }
                if (mpg.comboBox_Reservation_Movie.SelectedItem.ToString().Equals(mpg.comboBox_Reservation_Movie.Items[4].ToString()))
                {

                    mpg.comboBox_Reservation_Time.Items.Add("21:00");
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void Reserv_Places_Init()
        {

            if (mpg.comboBox_Reservation_Places_Week.Items.IsEmpty)
            {
                mpg.comboBox_Reservation_Places_Week.Items.Clear();
                mpg.comboBox_Reservation_Places_Week.Items.Add("Bieżący");
                mpg.comboBox_Reservation_Places_Week.Items.Add("Następny");
            }
        }
        public void Reserv_Day_Change()
        {
            if (mpg.comboBox_Places_Day.Items.IsEmpty && mpg.comboBox_Reservation_Places_Week.SelectedIndex!=-1)
            {
                mpg.comboBox_Places_Day.Items.Clear();
                mpg.comboBox_Places_Day.Items.Add("Poniedziałek");
                mpg.comboBox_Places_Day.Items.Add("Wtorek");
                mpg.comboBox_Places_Day.Items.Add("Środa");
                mpg.comboBox_Places_Day.Items.Add("Czwartek");
                mpg.comboBox_Places_Day.Items.Add("Piątek");
                mpg.comboBox_Places_Day.Items.Add("Sobota");
                mpg.comboBox_Places_Day.Items.Add("Niedziela");
            }
        }
        public void Reserv_Hours_Change()
        {
            if(mpg.comboBox_Places_Day.SelectedIndex!=-1)
            {
                mpg.Reservation_Places_Hour_M.Items.Clear();
                mpg.Reservation_Places_Hour_M.Items.Add("8:45");
                mpg.Reservation_Places_Hour_M.Items.Add("12:00");
                mpg.Reservation_Places_Hour_M.Items.Add("15:30");
                mpg.Reservation_Places_Hour_M.Items.Add("17:30");
                mpg.Reservation_Places_Hour_M.Items.Add("21:00");


                mpg.Reservation_Places_Hour_S.Items.Clear();
                mpg.Reservation_Places_Hour_S.Items.Add("8:45");
                mpg.Reservation_Places_Hour_S.Items.Add("12:00");
                mpg.Reservation_Places_Hour_S.Items.Add("15:30");
                mpg.Reservation_Places_Hour_S.Items.Add("17:30");
                mpg.Reservation_Places_Hour_S.Items.Add("21:00");


                mpg.Reservation_Places_Hour_B.Items.Clear();
                mpg.Reservation_Places_Hour_B.Items.Add("8:45");
                mpg.Reservation_Places_Hour_B.Items.Add("12:00");
                mpg.Reservation_Places_Hour_B.Items.Add("15:30");
                mpg.Reservation_Places_Hour_B.Items.Add("17:30");
                mpg.Reservation_Places_Hour_B.Items.Add("21:00");
            }
                
        }
        private Boolean DateRangeFunction1()
        {
            DateTime dat,start,stop = new DateTime();
            dat = DateTime.Now;
            start= DateTime.Now;
            if (dat.DayOfWeek == DayOfWeek.Monday)
            {
                start = DateTime.Now;
                stop = start.AddDays(6);
            }
            if (dat.DayOfWeek == DayOfWeek.Tuesday)
            {
                start = dat.AddDays(-1);
                stop = dat.AddDays(5);
            }
            if (dat.DayOfWeek == DayOfWeek.Wednesday)
            {
                start = dat.AddDays(-2);
                stop = dat.AddDays(4);
            }
            if (dat.DayOfWeek == DayOfWeek.Thursday)
            {
                start = dat.AddDays(-3);
                stop = dat.AddDays(3);
            }
            if (dat.DayOfWeek == DayOfWeek.Friday)
            {
                start = dat.AddDays(-4);
                stop = dat.AddDays(2);
            }
            if (dat.DayOfWeek == DayOfWeek.Saturday)
            {
                start = dat.AddDays(-5);
                stop = dat.AddDays(1);
            }
            if (dat.DayOfWeek == DayOfWeek.Sunday)
            {
                start = dat.AddDays(-6);
                stop = dat;
            }
            return (start <= dat && stop >= dat);
        }
        public void Reserv_Places(OleDbConnection myConnect, string room)
        {
            List<String> seater = new List<String>();
            myConnect.Open();
            string godzina="10:00";
            if (room.Equals("Mała"))
                 godzina=mpg.Reservation_Places_Hour_S.Text;
            if (room.Equals("Średnia"))
                 godzina=mpg.Reservation_Places_Hour_M.Text;
            if (room.Equals("Duża"))
                 godzina=mpg.Reservation_Places_Hour_B.Text;
            OleDbCommand cmd = new OleDbCommand("SELECT Miejsce FROM REZERWACJA_MIEJSC WHERE Sala='"+room+"' AND Godzina='"+godzina+"' AND Tydzien='"+ mpg.comboBox_Reservation_Places_Week.Text+"' AND Dzien='"+mpg.comboBox_Places_Day.Text+"'", myConnect);
            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                seater.Add(reader[0].ToString());
            }

            myConnect.Close();
            
            
            switch (room)
            {
                case "Duża":
                    {
                        for (int i = 0; i < seater.Count; i++)
                        {
                            
                            if (seater[i].Equals("A1")) mpg.A1_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("A2")) mpg.A2_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("A3")) mpg.A3_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("A4")) mpg.A4_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("A5")) mpg.A5_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("A6")) mpg.A6_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("A7")) mpg.A7_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("A8")) mpg.A8_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("A9")) mpg.A9_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("B1")) mpg.B1_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("B2")) mpg.B2_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("B3")) mpg.B3_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("B4")) mpg.B4_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("B5")) mpg.B5_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("B6")) mpg.B6_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("B7")) mpg.B7_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("B8")) mpg.B8_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("C1")) mpg.C1_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("C2")) mpg.C2_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("C3")) mpg.C3_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("C4")) mpg.C4_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("C5")) mpg.C5_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("C6")) mpg.C6_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("C7")) mpg.C7_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("D1")) mpg.D1_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("D2")) mpg.D2_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("D3")) mpg.D3_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("D4")) mpg.D4_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("D5")) mpg.D5_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("D6")) mpg.D6_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("E1")) mpg.E1_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("E2")) mpg.E2_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("E3")) mpg.E3_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("E4")) mpg.E4_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("E5")) mpg.E5_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("F1")) mpg.F1_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("F2")) mpg.F2_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("F3")) mpg.F3_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("F4")) mpg.F4_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("G1")) mpg.G1_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("G2")) mpg.G2_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("G3")) mpg.G3_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("G4")) mpg.G4_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("G5")) mpg.G5_B.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                        }
                    }
                    break;
                case "Średnia":
                    {
                        for (int i = 0; i < seater.Count; i++)
                        {
                            if (seater[i].Equals("A1")) mpg.A1_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("A2")) mpg.A2_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("A3")) mpg.A3_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("A4")) mpg.A4_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("A5")) mpg.A5_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("A6")) mpg.A6_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("A7")) mpg.A7_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("A8")) mpg.A8_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("B1")) mpg.B1_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("B2")) mpg.B2_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("B3")) mpg.B3_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("B4")) mpg.B4_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("B5")) mpg.B5_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("B6")) mpg.B6_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("B7")) mpg.B7_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("C1")) mpg.C1_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("C2")) mpg.C2_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("C3")) mpg.C3_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("C4")) mpg.C4_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("C5")) mpg.C5_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("C6")) mpg.C6_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("C7")) mpg.C7_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("D1")) mpg.D1_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("D2")) mpg.D2_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("D3")) mpg.D3_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("D4")) mpg.D4_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("D5")) mpg.D5_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("D6")) mpg.D6_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("D7")) mpg.D7_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("D8")) mpg.D8_M.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                        }
                    }
                    break;
                case "Mała":
                    {
                        for (int i = 0; i < seater.Count; i++)
                        {
                            if (seater[i].Equals("A2")) mpg.A2_S.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("A3")) mpg.A3_S.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("A4")) mpg.A4_S.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("A5")) mpg.A5_S.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("A6")) mpg.A6_S.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("A7")) mpg.A7_S.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("A8")) mpg.A8_S.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("B1")) mpg.B1_S.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("B2")) mpg.B2_S.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("B3")) mpg.B3_S.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("B4")) mpg.B4_S.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("B5")) mpg.B5_S.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("C1")) mpg.C1_S.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("C2")) mpg.C2_S.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("C3")) mpg.C3_S.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("C4")) mpg.C4_S.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("C5")) mpg.C5_S.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("D1")) mpg.D1_S.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("D2")) mpg.D2_S.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                            if (seater[i].Equals("D3")) mpg.D3_S.Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                        }
                    }
                    break;
                default:
                    break;
            }

        }
        public void Res_Places_S_INIT(OleDbConnection myConnect)
        {
                mpg.A2_S.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
                mpg.A3_S.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
                mpg.A4_S.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
                mpg.A5_S.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
                mpg.A6_S.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
                mpg.A7_S.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
                mpg.A8_S.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
                mpg.B1_S.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
                mpg.B2_S.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
                mpg.B3_S.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
                mpg.B4_S.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
                mpg.B5_S.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
                mpg.C1_S.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
                mpg.C2_S.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
                mpg.C3_S.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
                mpg.C4_S.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
                mpg.C5_S.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
                mpg.D1_S.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
                mpg.D2_S.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
                mpg.D3_S.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
          
               
        }
        public void Res_Places_M_INIT(OleDbConnection myConnect)
        {
            mpg.A1_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.A2_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.A3_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.A4_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.A5_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.A6_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.A7_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.A8_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.B1_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.B2_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.B3_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.B4_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.B5_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.B6_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.B7_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.C1_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.C2_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.C3_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.C4_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.C5_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.C6_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.C7_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.D1_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.D2_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.D3_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.D4_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.D5_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.D6_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.D7_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.D8_M.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
        }
        public void Res_Places_B_INIT(OleDbConnection myConnect)
        {
            mpg.A1_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.A2_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.A3_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.A4_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.A5_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.A6_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.A7_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.A8_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.A9_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.B1_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.B2_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.B3_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.B4_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.B5_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.B6_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.B7_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.B8_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.C1_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.C2_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.C3_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.C4_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.C5_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.C6_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.C7_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.D1_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.D2_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.D3_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.D4_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.D5_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.D6_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.E1_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.E2_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.E3_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.E4_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.E5_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.F1_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.F2_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.F3_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.F4_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.G1_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.G2_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.G3_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.G4_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
            mpg.G5_B.Fill = new SolidColorBrush(Color.FromRgb(22, 255, 64));
        }
    }
}
