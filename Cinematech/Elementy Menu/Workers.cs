﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Cinematech
{
    class Workers
    {
        MainPage mainpg;
        public Workers(MainPage mainpg)
        {
            this.mainpg = mainpg;
        }
        public bool Workers_Pass_Checker(String imie, String pass)
        {
            MainPage comp = new MainPage();
            String name = "Pracownik";
            String passwd = "12345";
            if (imie.Equals(name) && pass.Equals(passwd))
            {
                return true;
            }
            else
                return false;
        }

        public void DownloadCustomer()
        {
            OleDbConnection myConn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Cinematech.accdb;Persist Security Info=False;");
            myConn.Open();

            OleDbCommand myQuery = new OleDbCommand("SELECT COUNT([E-Mail])as [Wizyty], [E-Mail]  FROM DANE  GROUP BY[E-Mail] HAVING COUNT([E-Mail]) >= 2", myConn);
            myQuery.ExecuteNonQuery();
            OleDbDataAdapter myAdapter = new OleDbDataAdapter();
            myAdapter.SelectCommand = myQuery;

            DataTable d = new DataTable();
            myAdapter.Fill(d);

            DataSet ds = new DataSet();
            myAdapter.Fill(ds);

            myConn.Close();

            DataTable dt = new DataTable();
            dt.Columns.Add("E-Mail");
            dt.Columns.Add("Ilość Wizyt");
            
            


            IEnumerable Custom = mainpg.dataGrid_Workers_Client.ItemsSource;

            foreach (var Customers in Custom)
            {
                dt.Rows.Add((Customers as DataRowView).Row[0].ToString());
            }


            foreach (DataRow row2 in ds.Tables[0].Rows)
            {
                dt.Rows.Add(row2["E-Mail"].ToString(), row2["Wizyty"].ToString());
            }
            mainpg.dataGrid_Workers_Client.ItemsSource = dt.DefaultView;
        }



        public void Send(string item, string item2, string item3, string item4, string item5, DataTable dt)
        {

            OleDbConnection myConn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Cinematech.accdb;Persist Security Info=False;");
            myConn.Open();

            // ITEM
            OleDbCommand myQuery = new OleDbCommand("SELECT Fil.Nazwa AS[Nazwa], Fil.Czas_Trwania AS[Czas Trwania], Fg.Nazwa AS[Gatunek], Fil.Premiera AS [Premiera], Fp.Kraj AS[Produkcja], Tech.Typ As[Technologia] FROM(((Filmy as Fil  INNER JOIN FILMY_GATUNKI as Fg ON Fg.ID_GATUNKU = Fil.Gatunek) INNER JOIN FILMY_PRODUKCJA AS Fp ON Fil.Produkcja = Fp.ID_Produkcji) INNER JOIN FILMY_TECHNOLOGIA AS Tech ON Fil.Technologia = Tech.ID_Technologii) WHERE Fil.Nazwa = '" + item + "'", myConn);
            myQuery.ExecuteNonQuery();
            OleDbDataAdapter myAdapter = new OleDbDataAdapter();
            myAdapter.SelectCommand = myQuery;

            DataTable d = new DataTable();
            myAdapter.Fill(d);

            DataSet ds = new DataSet();
            myAdapter.Fill(ds);


            // ITEM2
            myQuery = new OleDbCommand("SELECT Fil.Nazwa AS[Nazwa], Fil.Czas_Trwania AS[Czas Trwania], Fg.Nazwa AS[Gatunek], Fil.Premiera AS [Premiera], Fp.Kraj AS[Produkcja], Tech.Typ As[Technologia] FROM(((Filmy as Fil  INNER JOIN FILMY_GATUNKI as Fg ON Fg.ID_GATUNKU = Fil.Gatunek) INNER JOIN FILMY_PRODUKCJA AS Fp ON Fil.Produkcja = Fp.ID_Produkcji) INNER JOIN FILMY_TECHNOLOGIA AS Tech ON Fil.Technologia = Tech.ID_Technologii) WHERE Fil.Nazwa = '" + item2 + "'", myConn);
            myQuery.ExecuteNonQuery();
            OleDbDataAdapter myAdapter2 = new OleDbDataAdapter();
            myAdapter2.SelectCommand = myQuery;

            DataTable d2 = new DataTable();
            myAdapter2.Fill(d2);


            DataSet ds2 = new DataSet();
            myAdapter2.Fill(ds2);

            //ITEM3
            myQuery = new OleDbCommand("SELECT Fil.Nazwa AS[Nazwa], Fil.Czas_Trwania AS[Czas Trwania], Fg.Nazwa AS[Gatunek], Fil.Premiera AS [Premiera], Fp.Kraj AS[Produkcja], Tech.Typ As[Technologia] FROM(((Filmy as Fil  INNER JOIN FILMY_GATUNKI as Fg ON Fg.ID_GATUNKU = Fil.Gatunek) INNER JOIN FILMY_PRODUKCJA AS Fp ON Fil.Produkcja = Fp.ID_Produkcji) INNER JOIN FILMY_TECHNOLOGIA AS Tech ON Fil.Technologia = Tech.ID_Technologii) WHERE Fil.Nazwa = '" + item3 + "'", myConn);
            myQuery.ExecuteNonQuery();
            OleDbDataAdapter myAdapter3 = new OleDbDataAdapter();
            myAdapter3.SelectCommand = myQuery;

            DataTable d3 = new DataTable();
            myAdapter3.Fill(d3);

            DataSet ds3 = new DataSet();
            myAdapter3.Fill(ds3);

            //ITEM4

            myQuery = new OleDbCommand("SELECT Fil.Nazwa AS[Nazwa], Fil.Czas_Trwania AS[Czas Trwania], Fg.Nazwa AS[Gatunek], Fil.Premiera AS [Premiera], Fp.Kraj AS[Produkcja], Tech.Typ As[Technologia] FROM(((Filmy as Fil  INNER JOIN FILMY_GATUNKI as Fg ON Fg.ID_GATUNKU = Fil.Gatunek) INNER JOIN FILMY_PRODUKCJA AS Fp ON Fil.Produkcja = Fp.ID_Produkcji) INNER JOIN FILMY_TECHNOLOGIA AS Tech ON Fil.Technologia = Tech.ID_Technologii) WHERE Fil.Nazwa = '" + item4 + "'", myConn);
            myQuery.ExecuteNonQuery();
            OleDbDataAdapter myAdapter4 = new OleDbDataAdapter();
            myAdapter4.SelectCommand = myQuery;

            DataTable d4 = new DataTable();
            myAdapter4.Fill(d4);


            DataSet ds4 = new DataSet();
            myAdapter4.Fill(ds4);

            //ITEM 5

            myQuery = new OleDbCommand("SELECT Fil.Nazwa AS[Nazwa], Fil.Czas_Trwania AS[Czas Trwania], Fg.Nazwa AS[Gatunek], Fil.Premiera AS [Premiera], Fp.Kraj AS[Produkcja], Tech.Typ As[Technologia] FROM(((Filmy as Fil  INNER JOIN FILMY_GATUNKI as Fg ON Fg.ID_GATUNKU = Fil.Gatunek) INNER JOIN FILMY_PRODUKCJA AS Fp ON Fil.Produkcja = Fp.ID_Produkcji) INNER JOIN FILMY_TECHNOLOGIA AS Tech ON Fil.Technologia = Tech.ID_Technologii) WHERE Fil.Nazwa = '" + item5 + "'", myConn);
            myQuery.ExecuteNonQuery();
            OleDbDataAdapter myAdapter5 = new OleDbDataAdapter();
            myAdapter5.SelectCommand = myQuery;

            DataTable d5 = new DataTable();
            myAdapter5.Fill(d5);


            DataSet ds5 = new DataSet();
            myAdapter5.Fill(ds5);

            myConn.Close();



            if (dt.Rows.Count < 5)
            {
                foreach (DataRow row2 in ds.Tables[0].Rows)
                {
                    dt.Rows.Add(row2["Nazwa"].ToString(), row2["Czas Trwania"].ToString() + " min", row2["Gatunek"].ToString(), row2["Premiera"].ToString(), row2["Produkcja"].ToString(), row2["Technologia"].ToString());
                }
                foreach (DataRow row2 in ds2.Tables[0].Rows)
                {
                    dt.Rows.Add(row2["Nazwa"].ToString(), row2["Czas Trwania"].ToString() + " min", row2["Gatunek"].ToString(), row2["Premiera"].ToString(), row2["Produkcja"].ToString(), row2["Technologia"].ToString());
                }

                foreach (DataRow row2 in ds3.Tables[0].Rows)
                {
                    dt.Rows.Add(row2["Nazwa"].ToString(), row2["Czas Trwania"].ToString() + " min", row2["Gatunek"].ToString(), row2["Premiera"].ToString(), row2["Produkcja"].ToString(), row2["Technologia"].ToString());
                }


                foreach (DataRow row2 in ds4.Tables[0].Rows)
                {
                    dt.Rows.Add(row2["Nazwa"].ToString(), row2["Czas Trwania"].ToString() + " min", row2["Gatunek"].ToString(), row2["Premiera"].ToString(), row2["Produkcja"].ToString(), row2["Technologia"].ToString());
                }

                foreach (DataRow row2 in ds5.Tables[0].Rows)
                {
                    dt.Rows.Add(row2["Nazwa"].ToString(), row2["Czas Trwania"].ToString() + " min", row2["Gatunek"].ToString(), row2["Premiera"].ToString(), row2["Produkcja"].ToString(), row2["Technologia"].ToString());
                }

                mainpg.Repertoir_Confirm_Message.Visibility = Visibility.Visible;
            }
            else
                MessageBox.Show("Już dodałeś filmy na ten dzień", "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);



        }

        public void Monday_Send()
        {

            var currentID = mainpg.Workers_Monday_Choose1.SelectedIndex;
            var currentID2 = mainpg.Workers_Monday_Choose2.SelectedIndex;
            var currentID3 = mainpg.Workers_Monday_Choose3.SelectedIndex;
            var currentID4 = mainpg.Workers_Monday_Choose4.SelectedIndex;
            var currentID5 = mainpg.Workers_Monday_Choose5.SelectedIndex;

            if ((currentID < 0) || (currentID2 < 0) || (currentID3 < 0) || (currentID4 < 0) || (currentID5 < 0))
            {
                MessageBox.Show("Proszę wypełnić wszystkie pola!!", "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                var item = mainpg.Workers_Monday_Choose1.Items.GetItemAt(currentID);
                var item2 = mainpg.Workers_Monday_Choose2.Items.GetItemAt(currentID2);
                var item3 = mainpg.Workers_Monday_Choose3.Items.GetItemAt(currentID3);
                var item4 = mainpg.Workers_Monday_Choose4.Items.GetItemAt(currentID4);
                var item5 = mainpg.Workers_Monday_Choose5.Items.GetItemAt(currentID5);

                DataTable dt = new DataTable();
                dt.Columns.Add("Nazwa filmu");
                dt.Columns.Add("Czas Trwania");
                dt.Columns.Add("Gatunek");
                dt.Columns.Add("Premiera");
                dt.Columns.Add("Produkcja");
                dt.Columns.Add("Technologia");

                IEnumerable movies = mainpg.Timetable_Mon.ItemsSource;

                foreach (var movie in movies)
                {
                    dt.Rows.Add((movie as DataRowView).Row[0].ToString());
                }

                Send(item.ToString(), item2.ToString(), item3.ToString(), item4.ToString(), item5.ToString(), dt);

                mainpg.Timetable_Mon.ItemsSource = dt.DefaultView;
                
                mainpg.Workers_Monday_Choose1.SelectedIndex = -1;
                mainpg.Workers_Monday_Choose2.SelectedIndex = -1;
                mainpg.Workers_Monday_Choose3.SelectedIndex = -1;
                mainpg.Workers_Monday_Choose4.SelectedIndex = -1;
                mainpg.Workers_Monday_Choose5.SelectedIndex = -1;
            }
        }

        public void Monday2_Send()
        {

            var currentID = mainpg.Workers_Monday2_Choose1.SelectedIndex;
            var currentID2 = mainpg.Workers_Monday2_Choose2.SelectedIndex;
            var currentID3 = mainpg.Workers_Monday2_Choose3.SelectedIndex;
            var currentID4 = mainpg.Workers_Monday2_Choose4.SelectedIndex;
            var currentID5 = mainpg.Workers_Monday2_Choose5.SelectedIndex;

            if ((currentID < 0) || (currentID2 < 0) || (currentID3 < 0) || (currentID4 < 0) || (currentID5 < 0))
            {
                MessageBox.Show("Proszę wypełnić wszystkie pola!!", "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                var item = mainpg.Workers_Monday2_Choose1.Items.GetItemAt(currentID);
                var item2 = mainpg.Workers_Monday2_Choose2.Items.GetItemAt(currentID2);
                var item3 = mainpg.Workers_Monday2_Choose3.Items.GetItemAt(currentID3);
                var item4 = mainpg.Workers_Monday2_Choose4.Items.GetItemAt(currentID4);
                var item5 = mainpg.Workers_Monday2_Choose5.Items.GetItemAt(currentID5);

                DataTable dt = new DataTable();
                dt.Columns.Add("Nazwa filmu");
                dt.Columns.Add("Czas Trwania");
                dt.Columns.Add("Gatunek");
                dt.Columns.Add("Premiera");
                dt.Columns.Add("Produkcja");
                dt.Columns.Add("Technologia");

                IEnumerable movies = mainpg.Timetable_Mon2.ItemsSource;

                foreach (var movie in movies)
                {
                    dt.Rows.Add((movie as DataRowView).Row[0].ToString());
                }

                Send(item.ToString(), item2.ToString(), item3.ToString(), item4.ToString(), item5.ToString(), dt);

                mainpg.Timetable_Mon2.ItemsSource = dt.DefaultView;
               
                
                mainpg.Workers_Monday2_Choose1.SelectedIndex = -1;
                mainpg.Workers_Monday2_Choose2.SelectedIndex = -1;
                mainpg.Workers_Monday2_Choose3.SelectedIndex = -1;
                mainpg.Workers_Monday2_Choose4.SelectedIndex = -1;
                mainpg.Workers_Monday2_Choose5.SelectedIndex = -1;
            }
        }

        public void Tuesday_Send()
        {
            var currentID = mainpg.Workers_Tuesday_Choose1.SelectedIndex;
            var currentID2 = mainpg.Workers_Tuesday_Choose2.SelectedIndex;
            var currentID3 = mainpg.Workers_Tuesday_Choose3.SelectedIndex;
            var currentID4 = mainpg.Workers_Tuesday_Choose4.SelectedIndex;
            var currentID5 = mainpg.Workers_Tuesday_Choose5.SelectedIndex;


            if ((currentID < 0) || (currentID2 < 0) || (currentID3 < 0) || (currentID4 < 0) || (currentID5 < 0))
            {
                MessageBox.Show("Proszę wypełnić wszystkie pola!!", "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                var item = mainpg.Workers_Tuesday_Choose1.Items.GetItemAt(currentID);
                var item2 = mainpg.Workers_Tuesday_Choose2.Items.GetItemAt(currentID2);
                var item3 = mainpg.Workers_Tuesday_Choose3.Items.GetItemAt(currentID3);
                var item4 = mainpg.Workers_Tuesday_Choose4.Items.GetItemAt(currentID4);
                var item5 = mainpg.Workers_Tuesday_Choose5.Items.GetItemAt(currentID5);


                DataTable dt = new DataTable();
                dt.Columns.Add("Nazwa filmu");
                dt.Columns.Add("Czas Trwania");
                dt.Columns.Add("Gatunek");
                dt.Columns.Add("Premiera");
                dt.Columns.Add("Produkcja");
                dt.Columns.Add("Technologia");

                IEnumerable movies = mainpg.Timetable_Tue.ItemsSource;

                foreach (var movie in movies)
                {
                    dt.Rows.Add((movie as DataRowView).Row[0].ToString());
                }

                Send(item.ToString(), item2.ToString(), item3.ToString(), item4.ToString(), item5.ToString(), dt);

                mainpg.Timetable_Tue.ItemsSource = dt.DefaultView;
        
                mainpg.Workers_Tuesday_Choose1.SelectedIndex = -1;
                mainpg.Workers_Tuesday_Choose2.SelectedIndex = -1;
                mainpg.Workers_Tuesday_Choose3.SelectedIndex = -1;
                mainpg.Workers_Tuesday_Choose4.SelectedIndex = -1;
                mainpg.Workers_Tuesday_Choose5.SelectedIndex = -1;
            }
        }

        public void Tuesday2_Send()
        {
            var currentID = mainpg.Workers_Tuesday2_Choose1.SelectedIndex;
            var currentID2 = mainpg.Workers_Tuesday2_Choose2.SelectedIndex;
            var currentID3 = mainpg.Workers_Tuesday2_Choose3.SelectedIndex;
            var currentID4 = mainpg.Workers_Tuesday2_Choose4.SelectedIndex;
            var currentID5 = mainpg.Workers_Tuesday2_Choose5.SelectedIndex;


            if ((currentID < 0) || (currentID2 < 0) || (currentID3 < 0) || (currentID4 < 0) || (currentID5 < 0))
            {
                MessageBox.Show("Proszę wypełnić wszystkie pola!!", "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                var item = mainpg.Workers_Tuesday2_Choose1.Items.GetItemAt(currentID);
                var item2 = mainpg.Workers_Tuesday2_Choose2.Items.GetItemAt(currentID2);
                var item3 = mainpg.Workers_Tuesday2_Choose3.Items.GetItemAt(currentID3);
                var item4 = mainpg.Workers_Tuesday2_Choose4.Items.GetItemAt(currentID4);
                var item5 = mainpg.Workers_Tuesday2_Choose5.Items.GetItemAt(currentID5);


                DataTable dt = new DataTable();
                dt.Columns.Add("Nazwa filmu");
                dt.Columns.Add("Czas Trwania");
                dt.Columns.Add("Gatunek");
                dt.Columns.Add("Premiera");
                dt.Columns.Add("Produkcja");
                dt.Columns.Add("Technologia");

                IEnumerable movies = mainpg.Timetable_Tue2.ItemsSource;

                foreach (var movie in movies)
                {
                    dt.Rows.Add((movie as DataRowView).Row[0].ToString());
                }

                Send(item.ToString(), item2.ToString(), item3.ToString(), item4.ToString(), item5.ToString(), dt);

                mainpg.Timetable_Tue2.ItemsSource = dt.DefaultView;

                mainpg.Workers_Tuesday2_Choose1.SelectedIndex = -1;
                mainpg.Workers_Tuesday2_Choose2.SelectedIndex = -1;
                mainpg.Workers_Tuesday2_Choose3.SelectedIndex = -1;
                mainpg.Workers_Tuesday2_Choose4.SelectedIndex = -1;
                mainpg.Workers_Tuesday2_Choose5.SelectedIndex = -1;
            }
        }

        public void Wednesday_Send()
        {
            var currentID = mainpg.Workers_Wednesday_Choose1.SelectedIndex;
            var currentID2 = mainpg.Workers_Wednesday_Choose2.SelectedIndex;
            var currentID3 = mainpg.Workers_Wednesday_Choose3.SelectedIndex;
            var currentID4 = mainpg.Workers_Wednesday_Choose4.SelectedIndex;
            var currentID5 = mainpg.Workers_Wednesday_Choose5.SelectedIndex;


            if ((currentID < 0) || (currentID2 < 0) || (currentID3 < 0) || (currentID4 < 0) || (currentID5 < 0))
            {
                MessageBox.Show("Proszę wypełnić wszystkie pola!!", "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                var item = mainpg.Workers_Wednesday_Choose1.Items.GetItemAt(currentID);
                var item2 = mainpg.Workers_Wednesday_Choose2.Items.GetItemAt(currentID2);
                var item3 = mainpg.Workers_Wednesday_Choose3.Items.GetItemAt(currentID3);
                var item4 = mainpg.Workers_Wednesday_Choose4.Items.GetItemAt(currentID4);
                var item5 = mainpg.Workers_Wednesday_Choose5.Items.GetItemAt(currentID5);


                DataTable dt = new DataTable();
                dt.Columns.Add("Nazwa filmu");
                dt.Columns.Add("Czas Trwania");
                dt.Columns.Add("Gatunek");
                dt.Columns.Add("Premiera");
                dt.Columns.Add("Produkcja");
                dt.Columns.Add("Technologia");

                IEnumerable movies = mainpg.Timetable_Wed.ItemsSource;

                foreach (var movie in movies)
                {
                    dt.Rows.Add((movie as DataRowView).Row[0].ToString());
                }

                Send(item.ToString(), item2.ToString(), item3.ToString(), item4.ToString(), item5.ToString(), dt);

                mainpg.Timetable_Wed.ItemsSource = dt.DefaultView;
               
                mainpg.Workers_Wednesday_Choose1.SelectedIndex = -1;
                mainpg.Workers_Wednesday_Choose2.SelectedIndex = -1;
                mainpg.Workers_Wednesday_Choose3.SelectedIndex = -1;
                mainpg.Workers_Wednesday_Choose4.SelectedIndex = -1;
                mainpg.Workers_Wednesday_Choose5.SelectedIndex = -1;

            }
        }

        public void Wednesday2_Send()
        {
            var currentID = mainpg.Workers_Wednesday2_Choose1.SelectedIndex;
            var currentID2 = mainpg.Workers_Wednesday2_Choose2.SelectedIndex;
            var currentID3 = mainpg.Workers_Wednesday2_Choose3.SelectedIndex;
            var currentID4 = mainpg.Workers_Wednesday2_Choose4.SelectedIndex;
            var currentID5 = mainpg.Workers_Wednesday2_Choose5.SelectedIndex;


            if ((currentID < 0) || (currentID2 < 0) || (currentID3 < 0) || (currentID4 < 0) || (currentID5 < 0))
            {
                MessageBox.Show("Proszę wypełnić wszystkie pola!!", "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                var item = mainpg.Workers_Wednesday2_Choose1.Items.GetItemAt(currentID);
                var item2 = mainpg.Workers_Wednesday2_Choose2.Items.GetItemAt(currentID2);
                var item3 = mainpg.Workers_Wednesday2_Choose3.Items.GetItemAt(currentID3);
                var item4 = mainpg.Workers_Wednesday2_Choose4.Items.GetItemAt(currentID4);
                var item5 = mainpg.Workers_Wednesday2_Choose5.Items.GetItemAt(currentID5);


                DataTable dt = new DataTable();
                dt.Columns.Add("Nazwa filmu");
                dt.Columns.Add("Czas Trwania");
                dt.Columns.Add("Gatunek");
                dt.Columns.Add("Premiera");
                dt.Columns.Add("Produkcja");
                dt.Columns.Add("Technologia");

                IEnumerable movies = mainpg.Timetable_Wed2.ItemsSource;

                foreach (var movie in movies)
                {
                    dt.Rows.Add((movie as DataRowView).Row[0].ToString());
                }

                Send(item.ToString(), item2.ToString(), item3.ToString(), item4.ToString(), item5.ToString(), dt);

                mainpg.Timetable_Wed2.ItemsSource = dt.DefaultView;

                mainpg.Workers_Wednesday2_Choose1.SelectedIndex = -1;
                mainpg.Workers_Wednesday2_Choose2.SelectedIndex = -1;
                mainpg.Workers_Wednesday2_Choose3.SelectedIndex = -1;
                mainpg.Workers_Wednesday2_Choose4.SelectedIndex = -1;
                mainpg.Workers_Wednesday2_Choose5.SelectedIndex = -1;

            }
        }

        public void Thursday_Send()
        {
            var currentID = mainpg.Workers_Thursday_Choose1.SelectedIndex;
            var currentID2 = mainpg.Workers_Thursday_Choose2.SelectedIndex;
            var currentID3 = mainpg.Workers_Thursday_Choose3.SelectedIndex;
            var currentID4 = mainpg.Workers_Thursday_Choose4.SelectedIndex;
            var currentID5 = mainpg.Workers_Thursday_Choose5.SelectedIndex;


            if ((currentID <= 0) || (currentID2 <= 0) || (currentID3 <= 0) || (currentID4 <= 0) || (currentID5 <= 0))
            {
                MessageBox.Show("Proszę wypełnić wszystkie pola!!", "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                var item = mainpg.Workers_Thursday_Choose1.Items.GetItemAt(currentID);
                var item2 = mainpg.Workers_Thursday_Choose2.Items.GetItemAt(currentID2);
                var item3 = mainpg.Workers_Thursday_Choose3.Items.GetItemAt(currentID3);
                var item4 = mainpg.Workers_Thursday_Choose4.Items.GetItemAt(currentID4);
                var item5 = mainpg.Workers_Thursday_Choose5.Items.GetItemAt(currentID5);


                DataTable dt = new DataTable();
                dt.Columns.Add("Nazwa filmu");
                dt.Columns.Add("Czas Trwania");
                dt.Columns.Add("Gatunek");
                dt.Columns.Add("Premiera");
                dt.Columns.Add("Produkcja");
                dt.Columns.Add("Technologia");

                IEnumerable movies = mainpg.Timetable_Thu.ItemsSource;

                foreach (var movie in movies)
                {
                    dt.Rows.Add((movie as DataRowView).Row[0].ToString());
                }

                Send(item.ToString(), item2.ToString(), item3.ToString(), item4.ToString(), item5.ToString(), dt);

                mainpg.Timetable_Thu.ItemsSource = dt.DefaultView;
              
                mainpg.Workers_Thursday_Choose1.SelectedIndex = -1;
                mainpg.Workers_Thursday_Choose2.SelectedIndex = -1;
                mainpg.Workers_Thursday_Choose3.SelectedIndex = -1;
                mainpg.Workers_Thursday_Choose4.SelectedIndex = -1;
                mainpg.Workers_Thursday_Choose5.SelectedIndex = -1;
            }
        }

        public void Thursday2_Send()
        {
            var currentID = mainpg.Workers_Thursday2_Choose1.SelectedIndex;
            var currentID2 = mainpg.Workers_Thursday2_Choose2.SelectedIndex;
            var currentID3 = mainpg.Workers_Thursday2_Choose3.SelectedIndex;
            var currentID4 = mainpg.Workers_Thursday2_Choose4.SelectedIndex;
            var currentID5 = mainpg.Workers_Thursday2_Choose5.SelectedIndex;


            if ((currentID <= 0) || (currentID2 <= 0) || (currentID3 <= 0) || (currentID4 <= 0) || (currentID5 <= 0))
            {
                MessageBox.Show("Proszę wypełnić wszystkie pola!!", "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                var item = mainpg.Workers_Thursday2_Choose1.Items.GetItemAt(currentID);
                var item2 = mainpg.Workers_Thursday2_Choose2.Items.GetItemAt(currentID2);
                var item3 = mainpg.Workers_Thursday2_Choose3.Items.GetItemAt(currentID3);
                var item4 = mainpg.Workers_Thursday2_Choose4.Items.GetItemAt(currentID4);
                var item5 = mainpg.Workers_Thursday2_Choose5.Items.GetItemAt(currentID5);


                DataTable dt = new DataTable();
                dt.Columns.Add("Nazwa filmu");
                dt.Columns.Add("Czas Trwania");
                dt.Columns.Add("Gatunek");
                dt.Columns.Add("Premiera");
                dt.Columns.Add("Produkcja");
                dt.Columns.Add("Technologia");

                IEnumerable movies = mainpg.Timetable_Thu2.ItemsSource;

                foreach (var movie in movies)
                {
                    dt.Rows.Add((movie as DataRowView).Row[0].ToString());
                }

                Send(item.ToString(), item2.ToString(), item3.ToString(), item4.ToString(), item5.ToString(), dt);

                mainpg.Timetable_Thu2.ItemsSource = dt.DefaultView;

                mainpg.Workers_Thursday2_Choose1.SelectedIndex = -1;
                mainpg.Workers_Thursday2_Choose2.SelectedIndex = -1;
                mainpg.Workers_Thursday2_Choose3.SelectedIndex = -1;
                mainpg.Workers_Thursday2_Choose4.SelectedIndex = -1;
                mainpg.Workers_Thursday2_Choose5.SelectedIndex = -1;
            }
        }

        public void Friday_Send()
        {
            var currentID = mainpg.Workers_Friday_Choose1.SelectedIndex;
            var currentID2 = mainpg.Workers_Friday_Choose2.SelectedIndex;
            var currentID3 = mainpg.Workers_Friday_Choose3.SelectedIndex;
            var currentID4 = mainpg.Workers_Friday_Choose4.SelectedIndex;
            var currentID5 = mainpg.Workers_Friday_Choose5.SelectedIndex;


            if ((currentID < 0) || (currentID2 < 0) || (currentID3 < 0) || (currentID4 < 0) || (currentID5 < 0))
            {
                MessageBox.Show("Proszę wypełnić wszystkie pola!!", "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                var item = mainpg.Workers_Friday_Choose1.Items.GetItemAt(currentID);
                var item2 = mainpg.Workers_Friday_Choose2.Items.GetItemAt(currentID2);
                var item3 = mainpg.Workers_Friday_Choose3.Items.GetItemAt(currentID3);
                var item4 = mainpg.Workers_Friday_Choose4.Items.GetItemAt(currentID4);
                var item5 = mainpg.Workers_Friday_Choose5.Items.GetItemAt(currentID5);

                DataTable dt = new DataTable();
                dt.Columns.Add("Nazwa filmu");
                dt.Columns.Add("Czas Trwania");
                dt.Columns.Add("Gatunek");
                dt.Columns.Add("Premiera");
                dt.Columns.Add("Produkcja");
                dt.Columns.Add("Technologia");

                IEnumerable movies = mainpg.Timetable_Fri.ItemsSource;

                foreach (var movie in movies)
                {
                    dt.Rows.Add((movie as DataRowView).Row[0].ToString());
                }

                Send(item.ToString(), item2.ToString(), item3.ToString(), item4.ToString(), item5.ToString(), dt);

                mainpg.Timetable_Fri.ItemsSource = dt.DefaultView;
                
                mainpg.Workers_Friday_Choose1.SelectedIndex = -1;
                mainpg.Workers_Friday_Choose2.SelectedIndex = -1;
                mainpg.Workers_Friday_Choose3.SelectedIndex = -1;
                mainpg.Workers_Friday_Choose4.SelectedIndex = -1;
                mainpg.Workers_Friday_Choose5.SelectedIndex = -1;
            }
        }

        public void Friday2_Send()
        {
            var currentID = mainpg.Workers_Friday2_Choose1.SelectedIndex;
            var currentID2 = mainpg.Workers_Friday2_Choose2.SelectedIndex;
            var currentID3 = mainpg.Workers_Friday2_Choose3.SelectedIndex;
            var currentID4 = mainpg.Workers_Friday2_Choose4.SelectedIndex;
            var currentID5 = mainpg.Workers_Friday2_Choose5.SelectedIndex;


            if ((currentID < 0) || (currentID2 < 0) || (currentID3 < 0) || (currentID4 < 0) || (currentID5 < 0))
            {
                MessageBox.Show("Proszę wypełnić wszystkie pola!!", "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                var item = mainpg.Workers_Friday2_Choose1.Items.GetItemAt(currentID);
                var item2 = mainpg.Workers_Friday2_Choose2.Items.GetItemAt(currentID2);
                var item3 = mainpg.Workers_Friday2_Choose3.Items.GetItemAt(currentID3);
                var item4 = mainpg.Workers_Friday2_Choose4.Items.GetItemAt(currentID4);
                var item5 = mainpg.Workers_Friday2_Choose5.Items.GetItemAt(currentID5);

                DataTable dt = new DataTable();
                dt.Columns.Add("Nazwa filmu");
                dt.Columns.Add("Czas Trwania");
                dt.Columns.Add("Gatunek");
                dt.Columns.Add("Premiera");
                dt.Columns.Add("Produkcja");
                dt.Columns.Add("Technologia");

                IEnumerable movies = mainpg.Timetable_Fri2.ItemsSource;

                foreach (var movie in movies)
                {
                    dt.Rows.Add((movie as DataRowView).Row[0].ToString());
                }

                Send(item.ToString(), item2.ToString(), item3.ToString(), item4.ToString(), item5.ToString(), dt);

                mainpg.Timetable_Fri2.ItemsSource = dt.DefaultView;
                
                mainpg.Workers_Friday2_Choose1.SelectedIndex = -1;
                mainpg.Workers_Friday2_Choose2.SelectedIndex = -1;
                mainpg.Workers_Friday2_Choose3.SelectedIndex = -1;
                mainpg.Workers_Friday2_Choose4.SelectedIndex = -1;
                mainpg.Workers_Friday2_Choose5.SelectedIndex = -1;
            }
        }

        public void Saturday_Send()
        {
            var currentID = mainpg.Workers_Saturday_Choose1.SelectedIndex;
            var currentID2 = mainpg.Workers_Saturday_Choose2.SelectedIndex;
            var currentID3 = mainpg.Workers_Saturday_Choose3.SelectedIndex;
            var currentID4 = mainpg.Workers_Saturday_Choose4.SelectedIndex;
            var currentID5 = mainpg.Workers_Saturday_Choose5.SelectedIndex;


            if ((currentID < 0) || (currentID2 < 0) || (currentID3 < 0) || (currentID4 < 0) || (currentID5 < 0))
            {
                MessageBox.Show("Proszę wypełnić wszystkie pola!!", "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                var item = mainpg.Workers_Saturday_Choose1.Items.GetItemAt(currentID);
                var item2 = mainpg.Workers_Saturday_Choose2.Items.GetItemAt(currentID2);
                var item3 = mainpg.Workers_Saturday_Choose3.Items.GetItemAt(currentID3);
                var item4 = mainpg.Workers_Saturday_Choose4.Items.GetItemAt(currentID4);
                var item5 = mainpg.Workers_Saturday_Choose5.Items.GetItemAt(currentID5);


                DataTable dt = new DataTable();
                dt.Columns.Add("Nazwa filmu");
                dt.Columns.Add("Czas Trwania");
                dt.Columns.Add("Gatunek");
                dt.Columns.Add("Premiera");
                dt.Columns.Add("Produkcja");
                dt.Columns.Add("Technologia");

                IEnumerable movies = mainpg.Timetable_Sat.ItemsSource;

                foreach (var movie in movies)
                {
                    dt.Rows.Add((movie as DataRowView).Row[0].ToString());
                }

                Send(item.ToString(), item2.ToString(), item3.ToString(), item4.ToString(), item5.ToString(), dt);

                mainpg.Timetable_Sat.ItemsSource = dt.DefaultView;
                
                mainpg.Workers_Saturday_Choose1.SelectedIndex = -1;
                mainpg.Workers_Saturday_Choose2.SelectedIndex = -1;
                mainpg.Workers_Saturday_Choose3.SelectedIndex = -1;
                mainpg.Workers_Saturday_Choose4.SelectedIndex = -1;
                mainpg.Workers_Saturday_Choose5.SelectedIndex = -1;
            }
        }

        public void Saturday2_Send()
        {
            var currentID = mainpg.Workers_Saturday2_Choose1.SelectedIndex;
            var currentID2 = mainpg.Workers_Saturday2_Choose2.SelectedIndex;
            var currentID3 = mainpg.Workers_Saturday2_Choose3.SelectedIndex;
            var currentID4 = mainpg.Workers_Saturday2_Choose4.SelectedIndex;
            var currentID5 = mainpg.Workers_Saturday2_Choose5.SelectedIndex;


            if ((currentID < 0) || (currentID2 < 0) || (currentID3 < 0) || (currentID4 < 0) || (currentID5 < 0))
            {
                MessageBox.Show("Proszę wypełnić wszystkie pola!!", "Informacja",MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                var item = mainpg.Workers_Saturday2_Choose1.Items.GetItemAt(currentID);
                var item2 = mainpg.Workers_Saturday2_Choose2.Items.GetItemAt(currentID2);
                var item3 = mainpg.Workers_Saturday2_Choose3.Items.GetItemAt(currentID3);
                var item4 = mainpg.Workers_Saturday2_Choose4.Items.GetItemAt(currentID4);
                var item5 = mainpg.Workers_Saturday2_Choose5.Items.GetItemAt(currentID5);


                DataTable dt = new DataTable();
                dt.Columns.Add("Nazwa filmu");
                dt.Columns.Add("Czas Trwania");
                dt.Columns.Add("Gatunek");
                dt.Columns.Add("Premiera");
                dt.Columns.Add("Produkcja");
                dt.Columns.Add("Technologia");

                IEnumerable movies = mainpg.Timetable_Sat2.ItemsSource;

                foreach (var movie in movies)
                {
                    dt.Rows.Add((movie as DataRowView).Row[0].ToString());
                }

                Send(item.ToString(), item2.ToString(), item3.ToString(), item4.ToString(), item5.ToString(), dt);

                mainpg.Timetable_Sat2.ItemsSource = dt.DefaultView;
              
                mainpg.Workers_Saturday2_Choose1.SelectedIndex = -1;
                mainpg.Workers_Saturday2_Choose2.SelectedIndex = -1;
                mainpg.Workers_Saturday2_Choose3.SelectedIndex = -1;
                mainpg.Workers_Saturday2_Choose4.SelectedIndex = -1;
                mainpg.Workers_Saturday2_Choose5.SelectedIndex = -1;
            }
        }

        public void Sunday_Send()
        {
            var currentID = mainpg.Workers_Sunday_Choose1.SelectedIndex;
            var currentID2 = mainpg.Workers_Sunday_Choose2.SelectedIndex;
            var currentID3 = mainpg.Workers_Sunday_Choose3.SelectedIndex;
            var currentID4 = mainpg.Workers_Sunday_Choose4.SelectedIndex;
            var currentID5 = mainpg.Workers_Sunday_Choose5.SelectedIndex;


            if ((currentID < 0) || (currentID2 < 0) || (currentID3 < 0) || (currentID4 < 0) || (currentID5 < 0))
            {
                MessageBox.Show("Proszę wypełnić wszystkie pola!!", "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                var item = mainpg.Workers_Sunday_Choose1.Items.GetItemAt(currentID);
                var item2 = mainpg.Workers_Sunday_Choose2.Items.GetItemAt(currentID2);
                var item3 = mainpg.Workers_Sunday_Choose3.Items.GetItemAt(currentID3);
                var item4 = mainpg.Workers_Sunday_Choose4.Items.GetItemAt(currentID4);
                var item5 = mainpg.Workers_Sunday_Choose5.Items.GetItemAt(currentID5);

                DataTable dt = new DataTable();
                dt.Columns.Add("Nazwa filmu");
                dt.Columns.Add("Czas Trwania");
                dt.Columns.Add("Gatunek");
                dt.Columns.Add("Premiera");
                dt.Columns.Add("Produkcja");
                dt.Columns.Add("Technologia");

                IEnumerable movies = mainpg.Timetable_Sun.ItemsSource;

                foreach (var movie in movies)
                {
                    dt.Rows.Add((movie as DataRowView).Row[0].ToString());
                }

                Send(item.ToString(), item2.ToString(), item3.ToString(), item4.ToString(), item5.ToString(), dt);

                mainpg.Timetable_Sun.ItemsSource = dt.DefaultView;
                
                mainpg.Workers_Sunday_Choose1.SelectedIndex = -1;
                mainpg.Workers_Sunday_Choose2.SelectedIndex = -1;
                mainpg.Workers_Sunday_Choose3.SelectedIndex = -1;
                mainpg.Workers_Sunday_Choose4.SelectedIndex = -1;
                mainpg.Workers_Sunday_Choose5.SelectedIndex = -1;
            }
        }

        public void Sunday2_Send()
        {
            var currentID = mainpg.Workers_Sunday2_Choose1.SelectedIndex;
            var currentID2 = mainpg.Workers_Sunday2_Choose2.SelectedIndex;
            var currentID3 = mainpg.Workers_Sunday2_Choose3.SelectedIndex;
            var currentID4 = mainpg.Workers_Sunday2_Choose4.SelectedIndex;
            var currentID5 = mainpg.Workers_Sunday2_Choose5.SelectedIndex;


            if ((currentID < 0) || (currentID2 < 0) || (currentID3 < 0) || (currentID4 < 0) || (currentID5 < 0))
            {
                MessageBox.Show("Proszę wypełnić wszystkie pola!!", "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                var item = mainpg.Workers_Sunday2_Choose1.Items.GetItemAt(currentID);
                var item2 = mainpg.Workers_Sunday2_Choose2.Items.GetItemAt(currentID2);
                var item3 = mainpg.Workers_Sunday2_Choose3.Items.GetItemAt(currentID3);
                var item4 = mainpg.Workers_Sunday2_Choose4.Items.GetItemAt(currentID4);
                var item5 = mainpg.Workers_Sunday2_Choose5.Items.GetItemAt(currentID5);

                DataTable dt = new DataTable();
                dt.Columns.Add("Nazwa filmu");
                dt.Columns.Add("Czas Trwania");
                dt.Columns.Add("Gatunek");
                dt.Columns.Add("Premiera");
                dt.Columns.Add("Produkcja");
                dt.Columns.Add("Technologia");

                IEnumerable movies = mainpg.Timetable_Sun2.ItemsSource;

                foreach (var movie in movies)
                {
                    dt.Rows.Add((movie as DataRowView).Row[0].ToString());
                }

                Send(item.ToString(), item2.ToString(), item3.ToString(), item4.ToString(), item5.ToString(), dt);

                mainpg.Timetable_Sun2.ItemsSource = dt.DefaultView;
               
                mainpg.Workers_Sunday2_Choose1.SelectedIndex = -1;
                mainpg.Workers_Sunday2_Choose2.SelectedIndex = -1;
                mainpg.Workers_Sunday2_Choose3.SelectedIndex = -1;
                mainpg.Workers_Sunday2_Choose4.SelectedIndex = -1;
                mainpg.Workers_Sunday2_Choose5.SelectedIndex = -1;
            }
        }
        public void Generate_Report(OleDbConnection myConnect)
        {
            myConnect.Open();
            OleDbCommand myQuery = new OleDbCommand("SELECT * FROM REZERWACJA_MIEJSC ORDER BY Data_Rez", myConnect);
            myQuery.ExecuteNonQuery();
            OleDbDataAdapter myAdapter = new OleDbDataAdapter();
            myAdapter.SelectCommand = myQuery;

            DataTable rep = new DataTable();
            myAdapter.Fill(rep);
            mainpg.dataGrid_Workers_Report.ItemsSource = rep.DefaultView;
            myConnect.Close();
        }
    }
}
