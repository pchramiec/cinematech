﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinematech
{
    class TurnOffElement
    {
        MainPage glowna;
        //MainPage mainpg;
        public TurnOffElement(MainPage mainpg)
        {
            this.glowna = mainpg;
        }
        /*Wyłącz wszystko*/
        public void TurnOffAll()
        {
            glowna.comboBox_Places_Day.Items.Clear();
            glowna.comboBox_Reservation_Places_Week.Items.Clear();
            glowna.Reservation_Places_Hour_S.Items.Clear();
            glowna.Reservation_Places_Hour_M.Items.Clear();
            glowna.Reservation_Places_Hour_B.Items.Clear();
            glowna.comboBox_Day_Of_Week.Visibility = System.Windows.Visibility.Collapsed;
            glowna.MainPage_Image.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Repertoire_Label.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Repertoire_TodayIs.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Timetable_Mon.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Timetable_Tue.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Timetable_Wed.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Timetable_Thu.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Timetable_Fri.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Timetable_Sat.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Timetable_Sun.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Timetable_Mon2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Timetable_Tue2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Timetable_Wed2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Timetable_Thu2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Timetable_Fri2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Timetable_Sat2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Timetable_Sun2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Monday_Repertoire.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Tuesday_Repertoire.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Wednesday_Repertoire.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Thursday_Repertoire.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Friday_Repertoire.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Saturday_Repertoire.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Sunday_Repertoire.Visibility = System.Windows.Visibility.Collapsed;
            glowna.LogOn_Label.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Login_Label.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Password_Label.Visibility = System.Windows.Visibility.Collapsed;
            glowna.textBox_Login.Visibility = System.Windows.Visibility.Collapsed;
            glowna.passwordBox_Password.Visibility = System.Windows.Visibility.Collapsed;
            glowna.LogIn_Button.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Reservation_Booking.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Reservation_Places.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Workers_Repertoir.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Tuesday_Workers.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Monday_Workers.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Thursday_Workers.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Wednesday_Workers.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Friday_Workers.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Saturday_Workers.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Sunday_Workers.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Worskers_Repertoir_Monday_Header.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Repertoir_Hour_8_45.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Repertoir_Hour_12_00.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Repertoir_Hour_15_30.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Repertoir_Hour_17_30.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Repertoir_Hour_21_00.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Monday_Choose1.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Monday_Choose2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Monday_Choose3.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Monday_Choose4.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Monday_Choose5.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Monday_Send.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Repertoir_Tuesday_Header.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Tuesday_Choose1.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Tuesday_Choose2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Tuesday_Choose3.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Tuesday_Choose4.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Tuesday_Choose5.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Tuesday_Send.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Repertoir_Wednesday_Header.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Wednesday_Choose1.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Wednesday_Choose2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Wednesday_Choose3.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Wednesday_Choose4.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Wednesday_Choose5.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Wednesday_Send.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Repertoir_Thursday_Header.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Thursday_Choose1.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Thursday_Choose2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Thursday_Choose3.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Thursday_Choose4.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Thursday_Choose5.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Thursday_Send.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Repertoir_Friday_Header.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Friday_Choose1.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Friday_Choose2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Friday_Choose3.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Friday_Choose4.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Friday_Choose5.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Friday_Send.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Repertoir_Saturday_Header.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Saturday_Choose1.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Saturday_Choose2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Saturday_Choose3.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Saturday_Choose4.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Saturday_Choose5.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Saturday_Send.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Repertoir_Sunday_Header.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Sunday_Choose1.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Sunday_Choose2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Sunday_Choose3.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Sunday_Choose4.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Sunday_Choose5.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Sunday_Send.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Monday2_Choose1.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Monday2_Choose2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Monday2_Choose3.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Monday2_Choose4.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Monday2_Choose5.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Monday2_Send.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Tuesday2_Choose1.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Tuesday2_Choose2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Tuesday2_Choose3.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Tuesday2_Choose4.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Tuesday2_Choose5.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Tuesday2_Send.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Wednesday2_Choose1.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Wednesday2_Choose2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Wednesday2_Choose3.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Wednesday2_Choose4.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Wednesday2_Choose5.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Wednesday2_Send.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Thursday2_Choose1.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Thursday2_Choose2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Thursday2_Choose3.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Thursday2_Choose4.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Thursday2_Choose5.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Thursday2_Send.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Friday2_Choose1.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Friday2_Choose2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Friday2_Choose3.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Friday2_Choose4.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Friday2_Choose5.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Friday2_Send.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Saturday2_Choose1.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Saturday2_Choose2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Saturday2_Choose3.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Saturday2_Choose4.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Saturday2_Choose5.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Saturday2_Send.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Sunday2_Choose1.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Sunday2_Choose2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Sunday2_Choose3.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Sunday2_Choose4.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Sunday2_Choose5.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Workers_Sunday2_Send.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_small_room.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_medium_room.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_big_room.Visibility = System.Windows.Visibility.Collapsed;
            glowna.label_Reservation_FirstName.Visibility = System.Windows.Visibility.Collapsed;
            glowna.label_Reservation_Email.Visibility = System.Windows.Visibility.Collapsed;
            glowna.label_Reservation_Day.Visibility = System.Windows.Visibility.Collapsed;
            glowna.label_Reservation_Movie.Visibility = System.Windows.Visibility.Collapsed;
            glowna.label_Reservation_Time.Visibility = System.Windows.Visibility.Collapsed;
            glowna.label_Reservation_Room.Visibility = System.Windows.Visibility.Collapsed;
            glowna.textBox_Reservation_FirstName.Visibility = System.Windows.Visibility.Collapsed;
            glowna.textBox_Reservation_Email.Visibility = System.Windows.Visibility.Collapsed;
            glowna.comboBox_Reservation_Movie.Visibility = System.Windows.Visibility.Collapsed;
            glowna.comboBox_Reservation_Time.Visibility = System.Windows.Visibility.Collapsed;
            glowna.comboBox_Reservation_Room.Visibility = System.Windows.Visibility.Collapsed;
            glowna.label_Reservation_Seat.Visibility = System.Windows.Visibility.Collapsed;
            glowna.comboBox_Reservation_Seat.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Reservation_Send.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Reservation_Places_Hour_S.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Reservation_Places_Label_Hour.Visibility = System.Windows.Visibility.Collapsed;
            glowna.B1_S.Visibility = System.Windows.Visibility.Collapsed;
            glowna.B2_S.Visibility = System.Windows.Visibility.Collapsed;
            glowna.B3_S.Visibility = System.Windows.Visibility.Collapsed;
            glowna.B4_S.Visibility = System.Windows.Visibility.Collapsed;
            glowna.B5_S.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A2_S.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A3_S.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A4_S.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A5_S.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A6_S.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A7_S.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A8_S.Visibility = System.Windows.Visibility.Collapsed;
            glowna.C1_S.Visibility = System.Windows.Visibility.Collapsed;
            glowna.C2_S.Visibility = System.Windows.Visibility.Collapsed;
            glowna.C3_S.Visibility = System.Windows.Visibility.Collapsed;
            glowna.C4_S.Visibility = System.Windows.Visibility.Collapsed;
            glowna.C5_S.Visibility = System.Windows.Visibility.Collapsed;
            glowna.D1_S.Visibility = System.Windows.Visibility.Collapsed;
            glowna.D2_S.Visibility = System.Windows.Visibility.Collapsed;
            glowna.D3_S.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Screen.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Reservation_Places_Hour_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A1_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A2_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A3_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A4_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A5_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A6_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A7_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A8_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.B1_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.B2_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.B3_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.B4_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.B5_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.B6_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.B7_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.C1_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.C2_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.C3_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.C4_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.C5_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.C6_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.C7_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.C8_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.D1_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.D2_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.D3_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.D4_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.D5_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.D6_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.D7_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.D8_M.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Reservation_Places_Hour_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A1_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A2_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A3_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A4_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A5_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A6_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A7_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A8_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.A9_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.B1_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.B2_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.B3_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.B4_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.B5_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.B6_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.B7_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.B8_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.C1_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.C2_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.C3_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.C4_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.C5_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.C6_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.C7_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.D1_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.D2_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.D3_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.D4_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.D5_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.D6_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.E1_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.E2_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.E3_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.E4_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.E5_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.F1_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.F2_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.F3_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.F4_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.G1_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.G2_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.G3_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.G4_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.G5_B.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Repertoir_Confirm_Message.Visibility = System.Windows.Visibility.Collapsed;
            glowna.comboBox_Reservation_Prize.Visibility = System.Windows.Visibility.Collapsed;
            glowna.label_Reservation_Prize.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Workers_Report.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Workers_Client.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Workers_Report_Generate.Visibility = System.Windows.Visibility.Collapsed;
            glowna.dataGrid_Workers_Client.Visibility = System.Windows.Visibility.Collapsed;
            glowna.dataGrid_Workers_Report.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Workers_Client_Download.Visibility = System.Windows.Visibility.Collapsed;
            glowna.label_Contact_Name.Visibility = System.Windows.Visibility.Collapsed;
            glowna.textBox_Contact_Email.Visibility = System.Windows.Visibility.Collapsed;
            glowna.textBox_Contact_Content.Visibility = System.Windows.Visibility.Collapsed;
            glowna.label_Contact_Content.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Contact_Send.Visibility = System.Windows.Visibility.Collapsed;
            glowna.textBox_Contact_Address.Visibility = System.Windows.Visibility.Collapsed;
            glowna.textBox_Contact_Name.Visibility = System.Windows.Visibility.Collapsed;
            glowna._8_45_Repertoir.Visibility = System.Windows.Visibility.Collapsed;
            glowna._12_00__Repertoir.Visibility = System.Windows.Visibility.Collapsed;
            glowna._15_30_Repertoir.Visibility = System.Windows.Visibility.Collapsed;
            glowna._17_30_Repertoir.Visibility = System.Windows.Visibility.Collapsed;
            glowna._21_00_Repertoir.Visibility = System.Windows.Visibility.Collapsed;
            glowna.specialoffers_post1.Visibility = System.Windows.Visibility.Collapsed;
            glowna.specialoffers_post2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.label_Contact_Topic.Visibility = System.Windows.Visibility.Collapsed;
            glowna.label_Contact_Emailer.Visibility = System.Windows.Visibility.Collapsed;
            glowna.textBox_Contact_Email_reply.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Workers_Next_Week.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Tuesday_Workers_2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Monday_Workers_2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Thursday_Workers_2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Wednesday_Workers_2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Friday_Workers_2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Saturday_Workers_2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Sunday_Workers_2.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Workers_Basic_Week.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Next_Week_Rep.Visibility = System.Windows.Visibility.Collapsed;
            glowna.button_Basic_Week_Rep.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Monday2_Repertoire.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Tuesday2_Repertoire.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Wednesday2_Repertoire.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Thursday2_Repertoire.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Friday2_Repertoire.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Saturday2_Repertoire.Visibility = System.Windows.Visibility.Collapsed;
            glowna.Sunday2_Repertoire.Visibility = System.Windows.Visibility.Collapsed;
            glowna.comboBox_Reservation_Date.Visibility = System.Windows.Visibility.Collapsed;
            glowna.comboBox_Places_Day.Visibility = System.Windows.Visibility.Collapsed;
            glowna.comboBox_Reservation_Places_Week.Visibility = System.Windows.Visibility.Collapsed;
            glowna.label_Res_Places_Day.Visibility = System.Windows.Visibility.Collapsed;
            glowna.label_Res_Place_Week.Visibility = System.Windows.Visibility.Collapsed;
            glowna.label_Repertoir_Week.Visibility = System.Windows.Visibility.Collapsed;
            glowna.textBox_About.Visibility = System.Windows.Visibility.Collapsed;
            glowna.textBox_Week_Type.Visibility = System.Windows.Visibility.Collapsed;
        }
        /*Funkcje z Repertuaru*/
        public void Repertoir_Main()
        {
            glowna.Background_Other.Visibility = System.Windows.Visibility.Visible;
            glowna.Repertoire_TodayIs.Visibility = System.Windows.Visibility.Visible;
            glowna.Repertoire_Label.Visibility = System.Windows.Visibility.Visible;
            glowna.Timetable_Mon.Visibility = System.Windows.Visibility.Visible;
            Repertoir_Days();
        }
        public void Repertoir_Main2()
        {
            glowna.Background_Other.Visibility = System.Windows.Visibility.Visible;
            glowna.Repertoire_TodayIs.Visibility = System.Windows.Visibility.Visible;
            glowna.Repertoire_Label.Visibility = System.Windows.Visibility.Visible;
            glowna.Timetable_Mon2.Visibility = System.Windows.Visibility.Visible;
            Repertoir_Days2();

        }
        public void Repertoir_Days()
        {
            glowna.Monday_Repertoire.Visibility = System.Windows.Visibility.Visible;
            glowna.Tuesday_Repertoire.Visibility = System.Windows.Visibility.Visible;
            glowna.Wednesday_Repertoire.Visibility = System.Windows.Visibility.Visible;
            glowna.Thursday_Repertoire.Visibility = System.Windows.Visibility.Visible;
            glowna.Friday_Repertoire.Visibility = System.Windows.Visibility.Visible;
            glowna.Saturday_Repertoire.Visibility = System.Windows.Visibility.Visible;
            glowna.Sunday_Repertoire.Visibility = System.Windows.Visibility.Visible;
            glowna.button_Next_Week_Rep.Visibility = System.Windows.Visibility.Visible;
            glowna.label_Repertoir_Week.Visibility = System.Windows.Visibility.Visible;
            glowna.label_Repertoir_Week.Text = "Tydzień bieżący";
        }
        public void Repertoir_Days2()
        {
            glowna.Monday2_Repertoire.Visibility = System.Windows.Visibility.Visible;
            glowna.Tuesday2_Repertoire.Visibility = System.Windows.Visibility.Visible;
            glowna.Wednesday2_Repertoire.Visibility = System.Windows.Visibility.Visible;
            glowna.Thursday2_Repertoire.Visibility = System.Windows.Visibility.Visible;
            glowna.Friday2_Repertoire.Visibility = System.Windows.Visibility.Visible;
            glowna.Saturday2_Repertoire.Visibility = System.Windows.Visibility.Visible;
            glowna.Sunday2_Repertoire.Visibility = System.Windows.Visibility.Visible;
            glowna.button_Basic_Week_Rep.Visibility = System.Windows.Visibility.Visible;
            glowna.label_Repertoir_Week.Visibility = System.Windows.Visibility.Visible;
            glowna.label_Repertoir_Week.Text = "Tydzień następny";
        }
        public void Repertoir_Hours()
        {
            glowna._8_45_Repertoir.Visibility = System.Windows.Visibility.Visible;
            glowna._12_00__Repertoir.Visibility = System.Windows.Visibility.Visible;
            glowna._15_30_Repertoir.Visibility = System.Windows.Visibility.Visible;
            glowna._17_30_Repertoir.Visibility = System.Windows.Visibility.Visible;
            glowna._21_00_Repertoir.Visibility = System.Windows.Visibility.Visible;
        }
        /*Funkcje z Rezerwacji*/
        public void Reservation_Main()
        {
            glowna.Background_Other.Visibility = System.Windows.Visibility.Visible;
            glowna.Reservation_Booking.Visibility = System.Windows.Visibility.Visible;
            glowna.Reservation_Places.Visibility = System.Windows.Visibility.Visible;
        }
        public void Reservation_Book()
        {
            glowna.label_Reservation_FirstName.Visibility = System.Windows.Visibility.Visible; ;
            glowna.label_Reservation_Email.Visibility = System.Windows.Visibility.Visible; ;
            glowna.label_Reservation_Day.Visibility = System.Windows.Visibility.Visible; ;
            glowna.label_Reservation_Time.Visibility = System.Windows.Visibility.Visible; ;
            glowna.label_Reservation_Seat.Visibility = System.Windows.Visibility.Visible; ;
            glowna.label_Reservation_Room.Visibility = System.Windows.Visibility.Visible; ;
            glowna.label_Reservation_Movie.Visibility = System.Windows.Visibility.Visible; ;
            glowna.textBox_Reservation_Email.Visibility = System.Windows.Visibility.Visible; ;
            glowna.textBox_Reservation_FirstName.Visibility = System.Windows.Visibility.Visible; ;
            glowna.comboBox_Reservation_Date.Visibility = System.Windows.Visibility.Visible; ;
            glowna.comboBox_Reservation_Movie.Visibility = System.Windows.Visibility.Visible; ;
            glowna.comboBox_Reservation_Room.Visibility = System.Windows.Visibility.Visible; ;
            glowna.comboBox_Reservation_Time.Visibility = System.Windows.Visibility.Visible; ;
            glowna.comboBox_Reservation_Seat.Visibility = System.Windows.Visibility.Visible; ;
            glowna.button_Reservation_Send.Visibility = System.Windows.Visibility.Visible; ;
            glowna.label_Reservation_Prize.Visibility = System.Windows.Visibility.Visible; ;
            glowna.comboBox_Reservation_Prize.Visibility = System.Windows.Visibility.Visible;
            glowna.comboBox_Day_Of_Week.Visibility = System.Windows.Visibility.Visible;
        }
        public void Reservation_Place_Menu()
        {
            glowna.button_small_room.Visibility = System.Windows.Visibility.Visible;
            glowna.button_medium_room.Visibility = System.Windows.Visibility.Visible;
            glowna.button_big_room.Visibility = System.Windows.Visibility.Visible;
        }
        public void Reservation_Place_S()
        {
            glowna.A2_S.Visibility = System.Windows.Visibility.Visible; ;
            glowna.A3_S.Visibility = System.Windows.Visibility.Visible; ;
            glowna.A4_S.Visibility = System.Windows.Visibility.Visible; ;
            glowna.A5_S.Visibility = System.Windows.Visibility.Visible; ;
            glowna.A6_S.Visibility = System.Windows.Visibility.Visible; ;
            glowna.A7_S.Visibility = System.Windows.Visibility.Visible; ;
            glowna.A8_S.Visibility = System.Windows.Visibility.Visible; ;
            glowna.B1_S.Visibility = System.Windows.Visibility.Visible; ;
            glowna.B2_S.Visibility = System.Windows.Visibility.Visible; ;
            glowna.B3_S.Visibility = System.Windows.Visibility.Visible; ;
            glowna.B4_S.Visibility = System.Windows.Visibility.Visible; ;
            glowna.B5_S.Visibility = System.Windows.Visibility.Visible; ;
            glowna.C1_S.Visibility = System.Windows.Visibility.Visible; ;
            glowna.C2_S.Visibility = System.Windows.Visibility.Visible; ;
            glowna.C3_S.Visibility = System.Windows.Visibility.Visible; ;
            glowna.C4_S.Visibility = System.Windows.Visibility.Visible; ;
            glowna.C5_S.Visibility = System.Windows.Visibility.Visible; ;
            glowna.D1_S.Visibility = System.Windows.Visibility.Visible; ;
            glowna.D2_S.Visibility = System.Windows.Visibility.Visible; ;
            glowna.D3_S.Visibility = System.Windows.Visibility.Visible; ;
            glowna.Screen.Visibility = System.Windows.Visibility.Visible; ;
            glowna.Reservation_Places_Label_Hour.Visibility = System.Windows.Visibility.Visible; ;
            glowna.Reservation_Places_Hour_S.Visibility = System.Windows.Visibility.Visible; ;
            glowna.comboBox_Places_Day.Visibility = System.Windows.Visibility.Visible;
            glowna.comboBox_Reservation_Places_Week.Visibility = System.Windows.Visibility.Visible;
            glowna.label_Res_Places_Day.Visibility = System.Windows.Visibility.Visible;
            glowna.label_Res_Place_Week.Visibility = System.Windows.Visibility.Visible;
        }
        public void Reservation_Place_M()
        {
            glowna.A1_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.A2_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.A3_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.A4_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.A5_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.A6_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.A7_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.A8_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.B1_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.B2_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.B3_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.B4_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.B5_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.B6_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.B7_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.C1_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.C2_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.C3_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.C4_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.C5_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.C6_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.C7_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.C8_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.D1_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.D2_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.D3_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.D4_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.D5_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.D6_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.D7_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.D8_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.Reservation_Places_Hour_M.Visibility = System.Windows.Visibility.Visible; ;
            glowna.Reservation_Places_Label_Hour.Visibility = System.Windows.Visibility.Visible; ;
            glowna.Screen.Visibility = System.Windows.Visibility.Visible; ;
            glowna.comboBox_Places_Day.Visibility = System.Windows.Visibility.Visible;
            glowna.comboBox_Reservation_Places_Week.Visibility = System.Windows.Visibility.Visible;
            glowna.label_Res_Places_Day.Visibility = System.Windows.Visibility.Visible;
            glowna.label_Res_Place_Week.Visibility = System.Windows.Visibility.Visible;
        }
        public void Reservation_Place_B()
        {

            glowna.A1_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.A2_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.A3_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.A4_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.A5_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.A6_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.A7_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.A8_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.A9_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.B1_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.B2_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.B3_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.B4_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.B5_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.B6_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.B7_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.B8_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.C1_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.C2_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.C3_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.C4_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.C5_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.C6_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.C7_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.D1_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.D2_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.D3_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.D4_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.D5_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.D6_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.E1_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.E2_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.E3_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.E4_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.E5_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.F1_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.F2_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.F3_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.F4_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.G1_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.G2_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.G3_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.G4_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.G5_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.Reservation_Places_Hour_B.Visibility = System.Windows.Visibility.Visible; ;
            glowna.Reservation_Places_Label_Hour.Visibility = System.Windows.Visibility.Visible; ;
            glowna.Screen.Visibility = System.Windows.Visibility.Visible; ;
            glowna.comboBox_Places_Day.Visibility = System.Windows.Visibility.Visible;
            glowna.comboBox_Reservation_Places_Week.Visibility = System.Windows.Visibility.Visible;
            glowna.label_Res_Places_Day.Visibility = System.Windows.Visibility.Visible;
            glowna.label_Res_Place_Week.Visibility = System.Windows.Visibility.Visible;
        }
        /*Funkcje z Pracowników*/
        public void Workers_Main()
        {

        }
        public void Workers_Repertoir(int number)
        {
            switch (number)
            {
                case 1:
                    {
                        glowna.Worskers_Repertoir_Monday_Header.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Monday_Choose1.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Monday_Choose2.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Monday_Choose3.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Monday_Choose4.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Monday_Choose5.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Monday_Send.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_8_45.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_12_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_15_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_17_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_21_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Text = "Bieżący tydzień";
                    }
                    break;
                case 2:
                    {
                        glowna.Workers_Repertoir_Tuesday_Header.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Tuesday_Choose1.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Tuesday_Choose2.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Tuesday_Choose3.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Tuesday_Choose4.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Tuesday_Choose5.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Tuesday_Send.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_8_45.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_12_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_15_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_17_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_21_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Text = "Bieżący tydzień";
                    }
                    break;
                case 3:
                    {
                        glowna.Workers_Repertoir_Wednesday_Header.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Wednesday_Choose1.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Wednesday_Choose2.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Wednesday_Choose3.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Wednesday_Choose4.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Wednesday_Choose5.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Wednesday_Send.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_8_45.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_12_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_15_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_17_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_21_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Text = "Bieżący tydzień";
                    }
                    break;
                case 4:
                    {
                        glowna.Workers_Repertoir_Thursday_Header.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Thursday_Choose1.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Thursday_Choose2.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Thursday_Choose3.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Thursday_Choose4.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Thursday_Choose5.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Thursday_Send.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_8_45.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_12_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_15_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_17_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_21_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Text = "Bieżący tydzień";
                    }
                    break;
                case 5:
                    {
                        glowna.Workers_Repertoir_Friday_Header.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Friday_Choose1.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Friday_Choose2.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Friday_Choose3.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Friday_Choose4.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Friday_Choose5.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Friday_Send.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_8_45.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_12_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_15_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_17_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_21_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Text = "Bieżący tydzień";
                    }
                    break;
                case 6:
                    {
                        glowna.Workers_Repertoir_Saturday_Header.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Saturday_Choose1.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Saturday_Choose2.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Saturday_Choose3.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Saturday_Choose4.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Saturday_Choose5.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Saturday_Send.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_8_45.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_12_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_15_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_17_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_21_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Text = "Bieżący tydzień";
                    }
                    break;
                case 7:
                    {
                        glowna.Workers_Repertoir_Sunday_Header.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Sunday_Choose1.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Sunday_Choose2.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Sunday_Choose3.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Sunday_Choose4.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Sunday_Choose5.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Sunday_Send.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_8_45.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_12_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_15_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_17_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_21_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Text = "Bieżący tydzień";
                    }
                    break;
            }
        }
        public void Workers_Repertoir2(int number)
        {
            switch (number)
            {
                case 1:
                    {
                        glowna.Worskers_Repertoir_Monday_Header.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Monday2_Choose1.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Monday2_Choose2.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Monday2_Choose3.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Monday2_Choose4.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Monday2_Choose5.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Monday2_Send.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_8_45.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_12_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_15_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_17_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_21_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Text = "Następny tydzień";
                    }
                    break;
                case 2:
                    {
                        glowna.Workers_Repertoir_Tuesday_Header.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Tuesday2_Choose1.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Tuesday2_Choose2.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Tuesday2_Choose3.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Tuesday2_Choose4.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Tuesday2_Choose5.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Tuesday2_Send.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_8_45.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_12_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_15_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_17_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_21_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Text = "Następny tydzień";
                    }
                    break;
                case 3:
                    {
                        glowna.Workers_Repertoir_Wednesday_Header.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Wednesday2_Choose1.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Wednesday2_Choose2.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Wednesday2_Choose3.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Wednesday2_Choose4.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Wednesday2_Choose5.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Wednesday2_Send.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_8_45.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_12_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_15_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_17_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_21_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Text = "Następny tydzień";
                    }
                    break;
                case 4:
                    {
                        glowna.Workers_Repertoir_Thursday_Header.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Thursday2_Choose1.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Thursday2_Choose2.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Thursday2_Choose3.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Thursday2_Choose4.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Thursday2_Choose5.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Thursday2_Send.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_8_45.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_12_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_15_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_17_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_21_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Text = "Następny tydzień";
                    }
                    break;
                case 5:
                    {
                        glowna.Workers_Repertoir_Friday_Header.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Friday2_Choose1.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Friday2_Choose2.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Friday2_Choose3.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Friday2_Choose4.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Friday2_Choose5.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Friday2_Send.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_8_45.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_12_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_15_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_17_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_21_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Text = "Następny tydzień";
                    }
                    break;
                case 6:
                    {
                        glowna.Workers_Repertoir_Saturday_Header.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Saturday2_Choose1.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Saturday2_Choose2.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Saturday2_Choose3.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Saturday2_Choose4.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Saturday2_Choose5.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Saturday2_Send.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_8_45.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_12_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_15_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_17_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_21_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Text = "Następny tydzień";
                    }
                    break;
                case 7:
                    {
                        glowna.Workers_Repertoir_Sunday_Header.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Sunday2_Choose1.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Sunday2_Choose2.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Sunday2_Choose3.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Sunday2_Choose4.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Sunday2_Choose5.Visibility = System.Windows.Visibility.Visible;
                        glowna.Workers_Sunday2_Send.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_8_45.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_12_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_15_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_17_30.Visibility = System.Windows.Visibility.Visible;
                        glowna.Repertoir_Hour_21_00.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Visibility = System.Windows.Visibility.Visible;
                        glowna.textBox_Week_Type.Text = "Następny tydzień";
                    }
                    break;
            }

        }
        public void Workers_Menu_Option(int number)
        {
            switch (number)
            {
                case 1:
                    {
                        glowna.button_Workers_Repertoir.Visibility = System.Windows.Visibility.Visible;
                        glowna.button_Workers_Report.Visibility = System.Windows.Visibility.Visible;
                        glowna.button_Workers_Client.Visibility = System.Windows.Visibility.Visible;
                    }
                    break;
                case 2:
                    {
                        glowna.button_Monday_Workers.Visibility = System.Windows.Visibility.Visible;
                        glowna.button_Tuesday_Workers.Visibility = System.Windows.Visibility.Visible;
                        glowna.button_Wednesday_Workers.Visibility = System.Windows.Visibility.Visible;
                        glowna.button_Thursday_Workers.Visibility = System.Windows.Visibility.Visible;
                        glowna.button_Friday_Workers.Visibility = System.Windows.Visibility.Visible;
                        glowna.button_Saturday_Workers.Visibility = System.Windows.Visibility.Visible;
                        glowna.button_Sunday_Workers.Visibility = System.Windows.Visibility.Visible;
                        glowna.button_Workers_Next_Week.Visibility = System.Windows.Visibility.Visible;
                    }
                    break;
                case 3:
                    {
                        glowna.button_Tuesday_Workers_2.Visibility = System.Windows.Visibility.Visible;
                        glowna.button_Monday_Workers_2.Visibility = System.Windows.Visibility.Visible;
                        glowna.button_Thursday_Workers_2.Visibility = System.Windows.Visibility.Visible; ;
                        glowna.button_Wednesday_Workers_2.Visibility = System.Windows.Visibility.Visible; ;
                        glowna.button_Friday_Workers_2.Visibility = System.Windows.Visibility.Visible; ;
                        glowna.button_Saturday_Workers_2.Visibility = System.Windows.Visibility.Visible; ;
                        glowna.button_Sunday_Workers_2.Visibility = System.Windows.Visibility.Visible;
                        glowna.button_Workers_Basic_Week.Visibility = System.Windows.Visibility.Visible;
                    }
                    break;
                default:
                    break;
            }


        }
        public void Workers_Report()
        {
            glowna.button_Workers_Report_Generate.Visibility = System.Windows.Visibility.Visible;
            glowna.dataGrid_Workers_Report.Visibility = System.Windows.Visibility.Visible;
        }
        public void Workers_Client()
        {
            glowna.button_Workers_Client_Download.Visibility = System.Windows.Visibility.Visible;
            glowna.dataGrid_Workers_Client.Visibility = System.Windows.Visibility.Visible;
        }
        public void Workers_Login()
        {
            glowna.Login_Label.Visibility = System.Windows.Visibility.Visible;
            glowna.LogOn_Label.Visibility = System.Windows.Visibility.Visible;
            glowna.textBox_Login.Visibility = System.Windows.Visibility.Visible;
            glowna.passwordBox_Password.Visibility = System.Windows.Visibility.Visible;
            glowna.Password_Label.Visibility = System.Windows.Visibility.Visible;
            glowna.LogIn_Button.Visibility = System.Windows.Visibility.Visible;
        }
        /*Funkcje z O nas*/
        public void About_us()
        {
            glowna.textBox_About.Visibility = System.Windows.Visibility.Visible;
        }
        /*Funkcje Kontakt*/
        public void Contact_Main()
        {
            glowna.label_Contact_Name.Visibility = System.Windows.Visibility.Visible;
            glowna.textBox_Contact_Email.Visibility = System.Windows.Visibility.Visible;
            glowna.textBox_Contact_Content.Visibility = System.Windows.Visibility.Visible;
            glowna.label_Contact_Content.Visibility = System.Windows.Visibility.Visible;
            glowna.label_Contact_Name.Visibility = System.Windows.Visibility.Visible;
            glowna.button_Contact_Send.Visibility = System.Windows.Visibility.Visible;
            glowna.textBox_Contact_Address.Visibility = System.Windows.Visibility.Visible;
            glowna.textBox_Contact_Name.Visibility = System.Windows.Visibility.Visible;
            glowna.Background_Other.Visibility = System.Windows.Visibility.Visible;
            glowna.label_Contact_Topic.Visibility = System.Windows.Visibility.Visible;
            glowna.label_Contact_Emailer.Visibility = System.Windows.Visibility.Visible;
            glowna.textBox_Contact_Email_reply.Visibility = System.Windows.Visibility.Visible;
        }
        /*Funkcje główne okno*/
        public void MainPg()
        {
            glowna.MainPage_Image.Visibility = System.Windows.Visibility.Visible;
            glowna.Background_Other.Visibility = System.Windows.Visibility.Hidden;
        }
    }
}
