﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinematech
{
    class PosterGenerator
    {
        MainPage mainpg;
        public PosterGenerator(MainPage mainpg)
        {
            this.mainpg = mainpg;
        }
        public void Generate()
        {
            Random rnd = new Random();
            int number = rnd.Next(1, 3);
            switch (number)
            {
                case 1:
                    {
                        
                        mainpg.specialoffers_post1.Visibility = System.Windows.Visibility.Visible;
                        break;
                    }

                case 2:
                    {
                        
                        mainpg.specialoffers_post2.Visibility = System.Windows.Visibility.Visible;
                        break;
                    }
                default:
                    break;
            }

        }
    }

}
