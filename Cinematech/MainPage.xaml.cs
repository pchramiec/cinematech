﻿using System.Windows;
using System;
using System.Timers;
using System.Data.OleDb;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Threading;
using System.Linq;

namespace Cinematech
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml

    public partial class MainPage : Window
    {

        Function rep_obj;//To jest repertuar tylko już nie chce zmieniać nazwy
        Workers work_obj;
        PosterGenerator spec_obj;
        TurnOffElement turnoff_obj;
        Contact contact_obj;
        AboutUs about_obj;
        Reservation reser_obj;
        OleDbConnection myConn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Cinematech.accdb;Persist Security Info=False;");
        public MainPage()
        {
            InitializeComponent();
            MainPage_Image.Visibility = Visibility.Visible;


            rep_obj = new Function(this);
            work_obj = new Workers(this);
            spec_obj = new PosterGenerator(this);
            turnoff_obj = new TurnOffElement(this);
            contact_obj = new Contact(this);
            about_obj = new AboutUs(this);
            reser_obj = new Reservation(this);

            Repertoire_TodayIs.Content = rep_obj.DataFormat();

            


            myConn.Open();
            OleDbCommand myQuery = new OleDbCommand("SELECT DISTINCT Nazwa FROM FILMY", myConn);
            myQuery.ExecuteNonQuery();
            OleDbDataAdapter myAdapter = new OleDbDataAdapter();
            myAdapter.SelectCommand = myQuery;

            DataTable dt = new DataTable();
            myAdapter.Fill(dt);
            //myAdapter.Update(dt);

            DataSet ds = new DataSet();
            myAdapter.Fill(ds);

            myConn.Close();

            DispatcherTimer timer = new DispatcherTimer();
            timer.Tick += dispatcher_timer_tick;
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Start();

            Repertoire_TodayIs.Content = rep_obj.DataFormat();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                //Monday
                Workers_Monday_Choose1.Items.Add(row["Nazwa"].ToString());
                Workers_Monday_Choose2.Items.Add(row["Nazwa"].ToString());
                Workers_Monday_Choose3.Items.Add(row["Nazwa"].ToString());
                Workers_Monday_Choose4.Items.Add(row["Nazwa"].ToString());
                Workers_Monday_Choose5.Items.Add(row["Nazwa"].ToString());
                Workers_Monday2_Choose1.Items.Add(row["Nazwa"].ToString());
                Workers_Monday2_Choose2.Items.Add(row["Nazwa"].ToString());
                Workers_Monday2_Choose3.Items.Add(row["Nazwa"].ToString());
                Workers_Monday2_Choose4.Items.Add(row["Nazwa"].ToString());
                Workers_Monday2_Choose5.Items.Add(row["Nazwa"].ToString());
                //Tuesday
                Workers_Tuesday_Choose1.Items.Add(row["Nazwa"].ToString());
                Workers_Tuesday_Choose2.Items.Add(row["Nazwa"].ToString());
                Workers_Tuesday_Choose3.Items.Add(row["Nazwa"].ToString());
                Workers_Tuesday_Choose4.Items.Add(row["Nazwa"].ToString());
                Workers_Tuesday_Choose5.Items.Add(row["Nazwa"].ToString());
                Workers_Tuesday2_Choose1.Items.Add(row["Nazwa"].ToString());
                Workers_Tuesday2_Choose2.Items.Add(row["Nazwa"].ToString());
                Workers_Tuesday2_Choose3.Items.Add(row["Nazwa"].ToString());
                Workers_Tuesday2_Choose4.Items.Add(row["Nazwa"].ToString());
                Workers_Tuesday2_Choose5.Items.Add(row["Nazwa"].ToString());
                //Wednesday
                Workers_Wednesday_Choose1.Items.Add(row["Nazwa"].ToString());
                Workers_Wednesday_Choose2.Items.Add(row["Nazwa"].ToString());
                Workers_Wednesday_Choose3.Items.Add(row["Nazwa"].ToString());
                Workers_Wednesday_Choose4.Items.Add(row["Nazwa"].ToString());
                Workers_Wednesday_Choose5.Items.Add(row["Nazwa"].ToString());
                Workers_Wednesday2_Choose1.Items.Add(row["Nazwa"].ToString());
                Workers_Wednesday2_Choose2.Items.Add(row["Nazwa"].ToString());
                Workers_Wednesday2_Choose3.Items.Add(row["Nazwa"].ToString());
                Workers_Wednesday2_Choose4.Items.Add(row["Nazwa"].ToString());
                Workers_Wednesday2_Choose5.Items.Add(row["Nazwa"].ToString());
                //Thursday
                Workers_Thursday_Choose1.Items.Add(row["Nazwa"].ToString());
                Workers_Thursday_Choose2.Items.Add(row["Nazwa"].ToString());
                Workers_Thursday_Choose3.Items.Add(row["Nazwa"].ToString());
                Workers_Thursday_Choose4.Items.Add(row["Nazwa"].ToString());
                Workers_Thursday_Choose5.Items.Add(row["Nazwa"].ToString());
                Workers_Thursday2_Choose1.Items.Add(row["Nazwa"].ToString());
                Workers_Thursday2_Choose2.Items.Add(row["Nazwa"].ToString());
                Workers_Thursday2_Choose3.Items.Add(row["Nazwa"].ToString());
                Workers_Thursday2_Choose4.Items.Add(row["Nazwa"].ToString());
                Workers_Thursday2_Choose5.Items.Add(row["Nazwa"].ToString());
                //Friday
                Workers_Friday_Choose1.Items.Add(row["Nazwa"].ToString());
                Workers_Friday_Choose2.Items.Add(row["Nazwa"].ToString());
                Workers_Friday_Choose3.Items.Add(row["Nazwa"].ToString());
                Workers_Friday_Choose4.Items.Add(row["Nazwa"].ToString());
                Workers_Friday_Choose5.Items.Add(row["Nazwa"].ToString());
                Workers_Friday2_Choose1.Items.Add(row["Nazwa"].ToString());
                Workers_Friday2_Choose2.Items.Add(row["Nazwa"].ToString());
                Workers_Friday2_Choose3.Items.Add(row["Nazwa"].ToString());
                Workers_Friday2_Choose4.Items.Add(row["Nazwa"].ToString());
                Workers_Friday2_Choose5.Items.Add(row["Nazwa"].ToString());
                //Saturday
                Workers_Saturday_Choose1.Items.Add(row["Nazwa"].ToString());
                Workers_Saturday_Choose2.Items.Add(row["Nazwa"].ToString());
                Workers_Saturday_Choose3.Items.Add(row["Nazwa"].ToString());
                Workers_Saturday_Choose4.Items.Add(row["Nazwa"].ToString());
                Workers_Saturday_Choose5.Items.Add(row["Nazwa"].ToString());
                Workers_Saturday2_Choose1.Items.Add(row["Nazwa"].ToString());
                Workers_Saturday2_Choose2.Items.Add(row["Nazwa"].ToString());
                Workers_Saturday2_Choose3.Items.Add(row["Nazwa"].ToString());
                Workers_Saturday2_Choose4.Items.Add(row["Nazwa"].ToString());
                Workers_Saturday2_Choose5.Items.Add(row["Nazwa"].ToString());
                //Sunday
                Workers_Sunday_Choose1.Items.Add(row["Nazwa"].ToString());
                Workers_Sunday_Choose2.Items.Add(row["Nazwa"].ToString());
                Workers_Sunday_Choose3.Items.Add(row["Nazwa"].ToString());
                Workers_Sunday_Choose4.Items.Add(row["Nazwa"].ToString());
                Workers_Sunday_Choose5.Items.Add(row["Nazwa"].ToString());
                Workers_Sunday2_Choose1.Items.Add(row["Nazwa"].ToString());
                Workers_Sunday2_Choose2.Items.Add(row["Nazwa"].ToString());
                Workers_Sunday2_Choose3.Items.Add(row["Nazwa"].ToString());
                Workers_Sunday2_Choose4.Items.Add(row["Nazwa"].ToString());
                Workers_Sunday2_Choose5.Items.Add(row["Nazwa"].ToString());
            }

            DataTable DtView = new DataTable();
            DtView.Columns.Add("Nazwa filmu");
            Timetable_Mon.ItemsSource = DtView.DefaultView;
            Timetable_Tue.ItemsSource = DtView.DefaultView;
            Timetable_Wed.ItemsSource = DtView.DefaultView;
            Timetable_Thu.ItemsSource = DtView.DefaultView;
            Timetable_Fri.ItemsSource = DtView.DefaultView;
            Timetable_Sat.ItemsSource = DtView.DefaultView;
            Timetable_Sun.ItemsSource = DtView.DefaultView;
            Timetable_Mon2.ItemsSource = DtView.DefaultView;
            Timetable_Tue2.ItemsSource = DtView.DefaultView;
            Timetable_Wed2.ItemsSource = DtView.DefaultView;
            Timetable_Thu2.ItemsSource = DtView.DefaultView;
            Timetable_Fri2.ItemsSource = DtView.DefaultView;
            Timetable_Sat2.ItemsSource = DtView.DefaultView;
            Timetable_Sun2.ItemsSource = DtView.DefaultView;
            DataTable Dt2View = new DataTable();
            Dt2View.Columns.Add("E-Mail");
            Dt2View.Columns.Add("Wizyty");
            dataGrid_Workers_Client.ItemsSource = Dt2View.DefaultView;
        }


        private void dispatcher_timer_tick(object sender, EventArgs e)
        {
            Repertoire_TodayIs.Content = rep_obj.DataFormat();

        }
        //Funkcje obsługi Menu Głównego
        public void Button_Repertoire_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Repertoir_Main();
            turnoff_obj.Repertoir_Days();
            turnoff_obj.Repertoir_Hours();
        }
        private void Button_MainPage_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.MainPg();
        }

        private void Button_Reservation_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Reservation_Main();
        }

        private void Button_Offers_Click(object sender, RoutedEventArgs e)
        {


            turnoff_obj.TurnOffAll();
            Background_Other.Visibility = Visibility.Visible;
            spec_obj.Generate();
        }

        private void Button_Worker_Click(object sender, RoutedEventArgs e)
        {
            Background_Other.Visibility = Visibility.Visible;
            turnoff_obj.TurnOffAll();
            turnoff_obj.Workers_Login();
        }

        private void Button_About_Us_Click(object sender, RoutedEventArgs e)
        {
            Background_Other.Visibility = Visibility.Visible;
            turnoff_obj.TurnOffAll();
            turnoff_obj.About_us();
            textBox_About.Text = "O nas ! \n\nAutorzy: Daniel Pilarczyk, Paweł Chramiec \n\nJesteśmy studentami Politechniki Śląskiej w Gliwicach na wydziale Elektrycznym na kierunku Informatyka. Projekt wykonaliśmy na zaliczenie przedmiotu Bazy Danych, projekt ten wykonaliśmy w około miesiąc.\n\nWykorzystane oprogramowanie: \n-BitBucket\n-SourceTree\n-Trello \n\n Nasze zainteresowania: \n-Informatyka \n-Elektronika\n-Sport\n-Muzyka\n-Film\n-Inteligentne budynki";
        }
        private void Button_Contact_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Contact_Main();
            textBox_Contact_Address.Text = "Kino Cinematech\nul. Kinowa 404\n11-111 Bazodanowa\n+48999111888\ncontact@cinematech.pl";
        }

        //Funkcje Obsługi Klawiszy Do Strefy Pracownika
        //Logowanie
        private void LogIn_Button_Click(object sender, RoutedEventArgs e)
        {
            String loggon = textBox_Login.Text;
            String passon = passwordBox_Password.Password.ToString();
            if (work_obj.Workers_Pass_Checker(loggon, passon))
            {
                passwordBox_Password.Password = "";
                turnoff_obj.TurnOffAll();
                turnoff_obj.Workers_Menu_Option(1);
            }
            else
                MessageBox.Show("Niepoprawne hasło", "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);

        }
        //Wyświetl Menu Po Zalogowaniu



        private void button_Workers_Repertoir_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Workers_Menu_Option(2);
        }
        private void button_Workers_Report_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Workers_Report();
        }

        private void button_Workers_Client_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Workers_Client();

        }
        private void button_Monday_Workers_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Workers_Repertoir(1);

        }

        private void button_Tuesday_Workers_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Workers_Repertoir(2);
        }

        private void button_Wednesday_Workers_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Workers_Repertoir(3);
        }

        private void button_Thursday_Workers_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Workers_Repertoir(4);
        }

        private void button_Friday_Workers_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Workers_Repertoir(5);
        }

        private void button_Saturday_Workers_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Workers_Repertoir(6);
        }

        private void button_Sunday_Workers_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Workers_Repertoir(7);
        }
        private void button_Monday_Workers_2_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Workers_Repertoir2(1);
        }

        private void button_Tuesday_Workers_2_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Workers_Repertoir2(2);
        }

        private void button_Wednesday_Workers_2_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Workers_Repertoir2(3);
        }

        private void button_Thursday_Workers_2_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Workers_Repertoir2(4);
        }

        private void button_Friday_Workers_2_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Workers_Repertoir2(5);
        }

        private void button_Saturday_Workers_2_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Workers_Repertoir2(6);
        }

        private void button_Sunday_Workers_2_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Workers_Repertoir2(7);
        }
        private void button_Workers_Next_Week_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Workers_Menu_Option(3);
        }
        private void button_Workers_Basic_Week_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Workers_Menu_Option(2);
        }
        //System rezerwacji
        private void Reservation_Places_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Reservation_Place_Menu();
        }

        private void button_small_room_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Reservation_Place_S();
            reser_obj.Reserv_Places_Init();
            reser_obj.Reserv_Places(myConn, "Mała");
        }

        private void button_medium_room_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Reservation_Place_M();
            reser_obj.Reserv_Places_Init();
            reser_obj.Reserv_Places(myConn, "Średnia");
        }

        private void button_big_room_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Reservation_Place_B();
            reser_obj.Reserv_Places_Init();
            reser_obj.Reserv_Places(myConn, "Duża");
        }
        private void Reservation_Booking_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            reser_obj.InitCombo(myConn);
            turnoff_obj.Reservation_Book();
        }
        // Wyślij filmy do repertuaru PONIEDZIAŁEK
        private void Workers_Monday_Send_Click(object sender, RoutedEventArgs e)
        {
            work_obj.Monday_Send();
        }
        // Wyślij filmy do repertuaru WTOREK
        private void Workers_Tuesday_Send_Click(object sender, RoutedEventArgs e)
        {
            work_obj.Tuesday_Send();
        }
        // Wyślij filmy do repertuaru ŚRODA
        private void Workers_Wednesday_Send_Click(object sender, RoutedEventArgs e)
        {
            work_obj.Wednesday_Send();
        }
        // Wyślij filmy do repertuaru CZWARTEK
        private void Workers_Thursday_Send_Click(object sender, RoutedEventArgs e)
        {
            work_obj.Thursday_Send();
        }
        // Wyślij filmy do repertuaru PIĄTEK
        private void Workers_Friday_Send_Click(object sender, RoutedEventArgs e)
        {
            work_obj.Friday_Send();
        }
        // Wyślij filmy do repertuaru SOBOTA
        private void Workers_Saturday_Send_Click(object sender, RoutedEventArgs e)
        {
            work_obj.Saturday_Send();
        }
        // Wyślij filmy do repertuaru NIEDZIELA
        private void Workers_Sunday_Send_Click(object sender, RoutedEventArgs e)
        {
            work_obj.Sunday_Send();
        }

        private void Workers_Monday2_Send_Click(object sender, RoutedEventArgs e)
        {
            work_obj.Monday2_Send();
        }

        private void Workers_Tuesday2_Send_Click(object sender, RoutedEventArgs e)
        {
            work_obj.Tuesday2_Send();
        }

        private void Workers_Wednesday2_Send_Click(object sender, RoutedEventArgs e)
        {
            work_obj.Wednesday2_Send();
        }

        private void Workers_Thursday2_Send_Click(object sender, RoutedEventArgs e)
        {
            work_obj.Thursday2_Send();
        }

        private void Workers_Friday2_Send_Click(object sender, RoutedEventArgs e)
        {
            work_obj.Friday2_Send();
        }

        private void Workers_Saturday2_Send_Click(object sender, RoutedEventArgs e)
        {
            work_obj.Saturday2_Send();
        }

        private void Workers_Sunday2_Send_Click(object sender, RoutedEventArgs e)
        {
            work_obj.Sunday2_Send();
        }

        //Repertuar
        //DataGrid po naciśnięciu dnia tygodnia

        private void Monday_Repertoire_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Repertoir_Days();
            turnoff_obj.Repertoir_Hours();
            Timetable_Mon.Visibility = Visibility.Visible;
            Repertoire_Label.Visibility = Visibility.Visible;
            Repertoire_TodayIs.Visibility = Visibility.Visible;
        }

        private void Tuesday_Repertoire_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Repertoir_Days();
            turnoff_obj.Repertoir_Hours();
            Timetable_Tue.Visibility = Visibility.Visible;
            Repertoire_Label.Visibility = Visibility.Visible;
            Repertoire_TodayIs.Visibility = Visibility.Visible;
        }

        private void Wednesday_Repertoire_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Repertoir_Days();
            turnoff_obj.Repertoir_Hours();
            Timetable_Wed.Visibility = Visibility.Visible;
            Repertoire_Label.Visibility = Visibility.Visible;
            Repertoire_TodayIs.Visibility = Visibility.Visible;
        }

        private void Thursday_Repertoire_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Repertoir_Days();
            turnoff_obj.Repertoir_Hours();
            Timetable_Thu.Visibility = Visibility.Visible;
            Repertoire_Label.Visibility = Visibility.Visible;
            Repertoire_TodayIs.Visibility = Visibility.Visible;
        }

        private void Friday_Repertoire_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Repertoir_Days();
            turnoff_obj.Repertoir_Hours();
            Timetable_Fri.Visibility = Visibility.Visible;
            Repertoire_Label.Visibility = Visibility.Visible;
            Repertoire_TodayIs.Visibility = Visibility.Visible;
        }

        private void Saturday_Repertoire_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Repertoir_Days();
            turnoff_obj.Repertoir_Hours();
            Timetable_Sat.Visibility = Visibility.Visible;
            Repertoire_Label.Visibility = Visibility.Visible;
            Repertoire_TodayIs.Visibility = Visibility.Visible;
        }

        private void Sunday_Repertoire_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Repertoir_Days();
            turnoff_obj.Repertoir_Hours();
            Timetable_Sun.Visibility = Visibility.Visible;
            Repertoire_Label.Visibility = Visibility.Visible;
            Repertoire_TodayIs.Visibility = Visibility.Visible;
        }
        private void Monday2_Repertoire_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Repertoir_Days2();
            turnoff_obj.Repertoir_Hours();
            Timetable_Mon2.Visibility = Visibility.Visible;
            Repertoire_Label.Visibility = Visibility.Visible;
            Repertoire_TodayIs.Visibility = Visibility.Visible;
        }

        private void Tuesday2_Repertoire_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Repertoir_Days2();
            turnoff_obj.Repertoir_Hours();
            Timetable_Tue2.Visibility = Visibility.Visible;
            Repertoire_Label.Visibility = Visibility.Visible;
            Repertoire_TodayIs.Visibility = Visibility.Visible;
        }

        private void Wednesday2_Repertoire_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Repertoir_Days2();
            turnoff_obj.Repertoir_Hours();
            Timetable_Wed2.Visibility = Visibility.Visible;
            Repertoire_Label.Visibility = Visibility.Visible;
            Repertoire_TodayIs.Visibility = Visibility.Visible;
        }

        private void Thursday2_Repertoire_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Repertoir_Days2();
            turnoff_obj.Repertoir_Hours();
            Timetable_Thu2.Visibility = Visibility.Visible;
            Repertoire_Label.Visibility = Visibility.Visible;
            Repertoire_TodayIs.Visibility = Visibility.Visible;
        }

        private void Friday2_Repertoire_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Repertoir_Days2();
            turnoff_obj.Repertoir_Hours();
            Timetable_Fri2.Visibility = Visibility.Visible;
            Repertoire_Label.Visibility = Visibility.Visible;
            Repertoire_TodayIs.Visibility = Visibility.Visible;
        }

        private void Saturday2_Repertoire_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Repertoir_Days2();
            turnoff_obj.Repertoir_Hours();
            Timetable_Sat2.Visibility = Visibility.Visible;
            Repertoire_Label.Visibility = Visibility.Visible;
            Repertoire_TodayIs.Visibility = Visibility.Visible;
        }

        private void Sunday2_Repertoire_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Repertoir_Days2();
            turnoff_obj.Repertoir_Hours();
            Timetable_Sun2.Visibility = Visibility.Visible;
            Repertoire_Label.Visibility = Visibility.Visible;
            Repertoire_TodayIs.Visibility = Visibility.Visible;
        }
        private void button_Next_Week_Rep_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Repertoir_Days2();
            turnoff_obj.Repertoir_Hours();
            Timetable_Mon2.Visibility = Visibility.Visible;
            Repertoire_Label.Visibility = Visibility.Visible;
            Repertoire_TodayIs.Visibility = Visibility.Visible;
        }

        private void button_Basic_Week_Rep_Click(object sender, RoutedEventArgs e)
        {
            turnoff_obj.TurnOffAll();
            turnoff_obj.Repertoir_Days();
            turnoff_obj.Repertoir_Hours();
            Timetable_Mon.Visibility = Visibility.Visible;
            Repertoire_Label.Visibility = Visibility.Visible;
            Repertoire_TodayIs.Visibility = Visibility.Visible;
        }
        private void button_Workers_Report_Generate_Click(object sender, RoutedEventArgs e)
        {
            work_obj.Generate_Report(myConn);
        }

        private void button_Contact_Send_Click(object sender, RoutedEventArgs e)
        {
            contact_obj.wyslij();
        }
        private void combobox_Reservation_Seat_Opened(object sender, EventArgs e)
        {
            reser_obj.InitCombo(myConn);
        }
        private void combobox_Reservation_Day(object sender, EventArgs e)
        {
            reser_obj.InitCombo(myConn);
        }
        private void combobox_Reservation_Changed(object sender, EventArgs e)
        {
            comboBox_Day_Of_Week.Items.Clear();
            reser_obj.InitCombo(myConn);
        }


        private void button_Reservation_Send_Click(object sender, RoutedEventArgs e)
        {
            reser_obj.MakeReservation(myConn);
        }
        private void combobox_Reservation_Movie_Changed(object sender, EventArgs e)
        {
            reser_obj.Resrv_Hour();

        }
        private void Res_Day_Change(object sender, EventArgs e)
        {
            reser_obj.Reserv_Day_Change();
        }
        private void Res_Hours_Change(object sender,EventArgs e)
        {
            reser_obj.Reserv_Hours_Change();
        }
        private void System_Place_S(object sender,EventArgs e)
        {
            reser_obj.Res_Places_S_INIT(myConn);
            reser_obj.Reserv_Places(myConn, "Mała");
        }
        private void System_Place_M(object sender, EventArgs e)
        {
            reser_obj.Res_Places_M_INIT(myConn);
            reser_obj.Reserv_Places(myConn, "Średnia");
        }
        private void System_Place_B(object sender, EventArgs e)
        {
            reser_obj.Res_Places_B_INIT(myConn);
            reser_obj.Reserv_Places(myConn, "Duża");
        }

        private void button_Workers_Client_Download_Click(object sender, RoutedEventArgs e)
        {
            work_obj.DownloadCustomer();
        }
    }
    }
